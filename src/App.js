import React, { Component } from 'react';

import { ApolloClient } from 'apollo-client';
import { HttpLink, InMemoryCache } from 'apollo-boost';
import Routes from './components/routes'
import { ApolloProvider } from 'react-apollo';
import auth from './auth/auth'
import './index.css'
// apollo setup
const cache = new InMemoryCache()
const link = new HttpLink({
  uri:'http://localhost:4000/graphql'
})

const client = new ApolloClient({
  cache,
  link
})
class App extends Component {
  render() {
    return (
      <div className="App">
        <ApolloProvider client={client}>
          <Routes auth={auth} />
        </ApolloProvider>
      </div>
    )
  }
}

export default App;
