

class Auth {
    constructor(){
        this.logado = localStorage.getItem('logado');
        this.user = {}
    }
    login(user) {
        localStorage.setItem('user', JSON.stringify(user))
        localStorage.setItem('logado', true)
        this.logado = true
    }

    logout() {
        localStorage.setItem('user', JSON.stringify({}))
        localStorage.setItem('logado', false)
        this.logado = false
    }

    currentUser(){
        return this.user
    }

    userLogado() {
        return this.logado
    }
}

export default new Auth();