import React, { Component } from 'react';
import Button from '../button'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showDetails: false,
            added: props.added
        }
    }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
    cardHorario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
    handleChange = () => {
        this.setState((prevState,props) => {
            return {added: !prevState.added}
        })
        this.props.handleChange(this.props.atividade)
     }
     seeActivity = () => {
        this.setState((prevState,props) =>{
            return { showDetails : !prevState.showDetails }
        })
     }
    render() {
        return (
            <div className="cardContainer">
                {this.state.added ? <Button dark={true} label="remover" action="remover" handleClick={this.handleChange} active={false}/> :  <Button dark={true} label="adicionar" action="adicionar" handleClick={this.handleChange} active={false}/> }
                
                <div className="cardInfo">
        {this.props.needDate ? <div className="dia"> this.props.atividade.data.split("-").reverse().join("/") </div>: null}
                    <div className="horario"> {this.cardHorario(this.props.inicio, this.props.atividade.horarioDeTermino)}</div>
                    <div className="nomeDaAtividade">{this.props.atividade.nome}</div>
                    <div className="tipo-materia">{this.props.atividade.tipo + ' - ' + this.props.atividade.materia.disciplina}</div>
                </div>
                <span className="seeActivityDetails underlineButton" onClick={this.seeActivity}>ver mais</span>
                {/* {showDetails ? <ActivityDetails /> : null} */}
            </div>
        )
    }
}

export default index;