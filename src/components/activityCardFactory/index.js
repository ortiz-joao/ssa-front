import React from 'react';
import ActivityCard from '../activityCardPattern'
import ActivityDetails from '../activityDetails'
import Button from '../button'
import ParticipanteCard from '../participanteCard'
const index = (props) => {
    const participantes = (atividade) => {
        if (atividade.presencas.length > 0) {
            return atividade.presencas.map(p => {
                if(p.papel != "responsavel") {
                    return <ParticipanteCard usuario={p.usuario} atividadeId={atividade.id}/>
                } else {
                    return null
                }
                
        })} else {
            return null
        }
    }
    const cardMaker = (p) => {
        if(p.papel == 'responsavel') {
                const part = participantes(p.atividade)
                const button = <Button dark={false} label="editar" action="editar" handleClick={() => {props.handleEditar(p.atividade.id)}} active={false}/>
                const leftInfo = <div>
                {<div><p className="title uppercase">limite de participantes</p><p className="uppercase infoItem">{p.atividade.limiteDeParticipantes + " vagas"}</p></div>}
                {p.nota != null ? <div><p className="title uppercase">nota</p><p className="infoTextItem">Peso da avaliação:{" "+p.atividade.notaMaxima}</p></div>: null}
                {<div><p className="title uppercase">descricao</p><p className="infoItem infoText">{p.atividade.descricao}</p></div>}
                </div>
                return <ActivityCard details={<ActivityDetails button={button} right={part} manage={true} left={leftInfo} atividade={p.atividade} data={p.atividade.data} modal={"gerenciar"}/>} key={p.id} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino} button={button}>
                    <p className="title">{p.atividade.nome}</p>
                    {p.atividade.percursoDaAtividade != null ?<p className="percurso">{p.atividade.percursoDaAtividade.nome}</p> : null}
                </ActivityCard>
        } else if(props.inscrito) {
            const button =  <Button dark={false} label="inscrito" action="cancelar" handleClick={() => {props.handleCancelar(p.atividade.id)}} active={false}/>
            return <ActivityCard details={<ActivityDetails button={button} atividade={p.atividade} data={p.atividade.data} modal={"ver mais"}/>} key={p.id} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino} button={button}>
                <p className="title">{p.atividade.nome}</p>
                {p.atividade.percursoDaAtividade != null ? <p className="percurso">{p.atividade.percursoDaAtividade.nome}</p> : null}
            </ActivityCard>
        } else if(props.disponivel){
            const button = <Button dark={false} label="participar" action="participar" handleClick={() => {props.handleSub(p.id)}} active={false}/>
            const details = <ActivityDetails button={button} atividadeId={p.id} data={p.data} modal={"ver mais"}/>
            return (
            <ActivityCard key={p.id} details={details} inicio={p.horarioInicio} termino={p.horarioTermino} button={button}>
                    <p className="title">{p.nome}</p>
                    {p.percursoDaAtividade != null ? <p className="percurso">{p.percursoDaAtividade.nome}</p> : null}
                </ActivityCard>
            )
        } else {
            
        }
    }
    return (
        cardMaker(props.presenca)
    );
}

export default index;