import React from 'react';
import './styles.css'
const index = (props) => {
    const addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
    const cardHorario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return addZero(i.getHours()) + ':' + addZero(i.getMinutes()) + ' - ' + addZero(f.getHours()) + ':' + addZero(f.getMinutes())
     }
    return (
        <div className="cardContainerPattern" >
            <div className="fitButton">
                {props.button}
            </div>
            <div className="info">
                <div className="infoAbsoulte">
                {props.needDate ? <p className="title"> {props.data.split("-").reverse().join("/")} </p>: null}
                <p className="title">{cardHorario(props.inicio,props.termino) }</p>
                {props.children}
                </div>
            </div>
            <div className="presencaInfo">
                {props.presencaInfo}
            </div>
            <div className="bottomRigth">
                {props.details}
            </div>
        </div>
    );
}

export default index;