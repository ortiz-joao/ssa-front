import React, { Component } from 'react';
import {graphql} from 'react-apollo';
import {atividade} from '../../queries/queries'
import './styles.css'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.props = props
    }
    countVagas(limite, presencas){
        console.log(presencas.length);
        return limite - (presencas.length) - 1
    }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
     materias = (disciplinas) => {
         return disciplinas.map(d => {
             return d.disciplina
         })
     }
    horario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
    responsavel = (presencas) => {
        const p = presencas.filter(p => {
            return p.papel == "responsavel"
        })
        if(p.length > 0) {
            return "responsavel - " + p[0].usuario.nomeCompleto
        }
        return null
    }
    render() {
        return (
            <div className="container">
                {this.state.show && !this.props.data.loading ? (
                    <div className="modal">
                        <section className="firstSection">
                    <p className="title uppercase">{this.props.data.atividade.data.split("-").reverse().join("/") + ' ' + this.horario(this.props.data.atividade.horarioInicio, this.props.data.atividade.horarioTermino)}</p>
                    <p className="title capitalized">{this.responsavel(this.props.data.atividade.presencas)}</p>
                    {this.props.data.atividade.percurso != null ? <p className="title uppercase">percurso</p> : null}
                    {this.props.data.atividade.percurso != null ? <span className="capitalized itemInfo">{this.props.data.atividade.percurso.nome}</span>: null}
                    {this.props.left}
                    {this.props.button}
                </section>
                <section className="secondSection">
                    <button className="closeModal" onClick={() => {this.setState((prevState, props) =>{return {show: !prevState.show}})}}>x</button>
                    {this.props.manage ? null : <div> <p className="title uppercase">vagas</p>
                    <p className="infoText">{this.countVagas(this.props.data.atividade.limiteDeParticipantes, this.props.data.atividade.presencas)}</p>
                    <p className="title ippercase">descrição</p>
                    <p className="textInfo">{this.props.data.atividade.descricao}</p> </div>}
                    {this.props.right}
                </section>
                    </div>
                ) : 
                <button className="underlineButton" onClick={() => {this.setState((prevState, props) =>{return {show: !prevState.show}})}}>{this.props.modal}</button>}
            </div> 
        );
    }
}

export default graphql(atividade,{options: (props) =>{return {variables:{atividadeId: props.atividadeId}}}})(index);