import React, { Component } from 'react';
import ParticipanteCard from '../participanteCard'
import Button from '../button'
import { graphql } from 'react-apollo';
import {atividade} from '../../queries/queries'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            responsavel: JSON.parse(localStorage.getItem('user'))
        }
        this.props = props
    }
    countVagas(limite, presencas){
        return (presencas.lenght - 1) - limite
    }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
     participantes = (presencas) => {
        if (presencas.length > 0) {
            return presencas.map(p => {
                if(p.papel != "responsavel") {
                    return <ParticipanteCard usuario={p.usuario} atividadeId={atividade.id}/>
                } else {
                    return null
                }
        })} else {
            return null
        }
    }
    horario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
     gotToPercurso = (id) => { 

     }
    render() {
        return (
            <div className="container">
                {!this.props.data.loading  ? (<div>
                <section className="firstSection">
                    <p className="title uppercase">{this.props.data.atividade.data.replace('-', '.') + ' ' + this.horario(this.props.data.atividade.horarioInicio, this.props.data.atividade.horarioTermino)}</p>
                    <p className="title">{this.props.data.atividade.tipo}</p>
                    <p className="title">{this.props.data.atividade.nome}</p>
                    <p className="subTitle">{this.state.responsavel.nomeCompleto}</p>
                    <p className="subTitle">{this.props.data.atividade.local}</p>
                    {this.props.data.atividade.percurso != null ? <div>
                        <p className="title uppercase">percurso</p>
                        <p className="infoText">{'- '+this.props.data.atividade.percurso.nome}</p>
                        <span onClick={this.goToPercurso(this.props.data.atividade.percurso.id)} className="seePresenca">ver percurso</span>
                    </div> : null }
                    <p className="title uppercase">limite de participantes</p>
                    <p className="infoText">{this.props.data.atividade.limiteDeParticipantes + ' vagas'}</p>
                    <p className="title uppercase">descrição</p>
                    <p className="infoDescription">{this.props.data.atividade.descricao}</p>
                    {this.props.data.atividade.tipo == 'avaliacao' ? <div><p className="title uppercase">nota</p>
                        <p className="infoDescription">{this.props.data.atividade.notaMaxima}</p></div>: null}
                    <Button dark={true} label={this.props.buttonLabel} action={this.props.buttonAction} handleClick={this.props.buttonHandleClick} active={false}/>
                </section>
                <section className="secondSection">
                    {this.participantes(this.props.data.atividade.presencas)}
                </section></div>) : null}
            </div> 
        );
    }
}

export default graphql(atividade,{options: (props) =>{return {variables:{atividadeId: props.atividade.id}}}})(index);