import React, { Component } from 'react';

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
    }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
    cardHorario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
     seeMore = () => {
        this.setState((prevState,props) =>{
            return { show : !prevState.show }
        })
     }
    render() {
        return (
            <div>
                <p className="title uppercase">{cardHorario(props.presenca.atividade.horarioDeInicio, props.presenca.atividade.horarioDeTermino)}</p>
                <p>{props.presenca.atividade.nome}</p>
                <button className="seeMore underlineButton">ver mais</button>
            </div>
        );
    }
}

export default index;