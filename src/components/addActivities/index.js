import React, { Component } from 'react';
import Card from '../activityCard'
import GetActivities from '../getActivities'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedOptions: [],
            show: false
        }
    }
    cards = () => {
        return this.state.selectedOptions.map(atividade => {
            return(<Card added={true} atividade={atividade} handleChange={this.removeOpcao}/>)
        })
    }
    removeOpcao = (id) => {
        let r = this.state.selectedOptions.filter(r => {
            return r !== id
        })
        this.setState({
            selectedOptions: r
        })
    }
    show = () => {
        this.setState((prevState, props) => {
            return {show : !prevState.show}
        })
    }
    handleChange = (e) => {
        this.setState({
            selectedOptions: e
        })
    }
    render() {
        return (
            <div className="OptionsContainer">
                <p className="title uppercase">{this.props.title}</p>
                {this.cards()}
                <span onClick={this.show}>adicionar</span>
                {this.state.show ? <GetActivities show={this.show} handleChange={this.handleChange} selectedActivities={this.state.selectedOptions}/> : null}
            </div>
        );
    }
}

export default index;
