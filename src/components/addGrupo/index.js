import React, { Component } from 'react';
import GetGrupos from '../getGrupo'
import Options from '../grupoCel'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedOptions: [],
            show: false
        }
    }
    removeOpcao = (id) => {
        let r = this.state.selectedOptions.filter(r => {
            return r.id !== id
        })
        this.setState({
            selectedOptions: r
        })
    }
    show = () => {
        this.setState((prevState, props) => {
            return {show : !prevState.show}
        })
    }
    handleChange = (e) => {
        this.setState((prevState, props) => {
            return {show: !prevState.show, selectedOptions: e}
        })
        this.props.handleChange(e)
    }
    render() {
        return (
            <div className="OptionsContainer">
                <p className="title uppercase">{this.props.title}</p>
                <Options selected={this.state.selectedOptions} removeOpcao={this.removeOpcao}/>
                {this.state.selectedOptions.length < this.props.limite ? <span onClick={this.show}>adicionar</span> : null}
                {this.state.show ? <GetGrupos options={this.props.options} handleSelect={this.handleChange} selectedOptions={this.state.selectedOptions}/> : null}
            </div>
        );
    }
}

export default index;
