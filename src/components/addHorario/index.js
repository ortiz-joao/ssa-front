import React, { Component } from 'react';
import MaskedInput from 'react-text-mask'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            horario: "",
            dia: ""
        }
    }
    handleHour = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleDay = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    render() {
        return (
            <div className="horario">
                    <MaskedInput mask={[/\d/,/\d/,':',/\d/,/\d/]} ref="hora" className="horarioText"   name="horario" value={this.state.horario} onChange={this.handleHour}/>
                    <MaskedInput mask={[/\d/,/\d/,'/',/\d/,/\d/]} ref="dia" className="horarioText"  name="dia" value={this.state.dia} onChange={this.handleDay}/>
                    <div className="actions">
                        <div className="action" onClick={() => {this.props.addHorario(this.state)}}>adicionar</div>
                    </div>
            </div>
        );
    }
}

export default index;