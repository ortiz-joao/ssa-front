import React, { Component } from 'react';
import GetOptions from '../getOptions'
import Options from '../optionCel'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedOptions: this.props.selectedOptions,
            show: false
        }
    }
    removeOpcao = (id) => {
        let r = this.state.selectedOptions.filter(r => {
            return r.id !== id
        })
        this.setState({
            selectedOptions: r
        })
    }
    show = () => {
        this.setState((prevState, props) => {
            return {show : !prevState.show}
        })
    }
    handleChange = (e) => {
        this.setState({
            selectedOptions: e
        })
        this.props.handleSelect(e)
    }
    render() {
        return (
            <div className="OptionsContainer">
                <p className="title uppercase">{this.props.title}</p>
                <Options selectedOptions={this.state.selectedOptions} label={this.props.optionLabel} id={this.props.optionId} removeOpcao={this.removeOpcao}/>
                <span onClick={this.show}>adicionar</span>
                {this.state.show ? <GetOptions show={this.show} options={this.props.options} handleSelect={this.handleChange} selectedOptions={this.state.selectedOptions}/> : null}
            </div>
        );
    }
}

export default index;
