import React from 'react';
import _ from 'lodash'
import {graphql} from 'react-apollo'
import {atividadesDoUsuario,removePresencaFromUser} from '../../queries/queries'
import ActivityCard from '../activityCardPattern'
import ActivityDetails from '../activityDetails'
import Button from '../button'
import DayRow from '../dayRow'
import ParticipanteCard from '../participanteCard'
import CardFactory from '../activityCardFactory'
const index = (props) => {
    const handleCancelar = (id) => {
        props.removePresenca({
            variables:{
                usuarioId: JSON.parse(localStorage.getItem('user')).id,
                atividadeId: parseInt(id)
            },
            refetchQueries: [{query: atividadesDoUsuario}]
        })
    }
    const handleEditar = (id) => {
        
    }
    const participantes = (atividade) => {
        if (atividade.presencas.length > 0) {
            return atividade.presencas.map(p => {
                if(p.papel != "responsavel") {
                    return <ParticipanteCard usuario={p.usuario} atividadeId={atividade.id}/>
                } else {
                    return null
                }
                
        })} else {
            return null
        }
    }
    const cardMaker = (p) => {
        if(p.papel == 'responsavel') {
                const part = participantes(p.atividade)
                const button = <Button dark={false} label="editar" action="editar" handleClick={() => {handleEditar(p.atividade.id)}} active={false}/>
                const buttonM = <Button dark={true} label="editar" action="editar" handleClick={() => {handleEditar(p.atividade.id)}} active={false}/>
                const leftInfo = <div>
                {<div><p className="title uppercase">limite de participantes</p><p className="uppercase infoItem">{p.atividade.limiteDeParticipantes + " vagas"}</p></div>}
                {p.nota != null ? <div><p className="title uppercase">nota</p><p className="infoTextItem">Peso da avaliação:{" "+p.atividade.notaMaxima}</p></div>: null}
                {<div><p className="title uppercase">descricao</p><p className="infoItem infoText">{p.atividade.descricao}</p></div>}
                </div>
                return <ActivityCard details={<ActivityDetails button={buttonM} right={part} manage={true} left={leftInfo} atividade={p.atividade} data={p.atividade.data} modal={"ver mais"}/>} key={p.id} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino} button={button}>
                    <p className="title">{p.atividade.nome}</p>
                    {p.atividade.percursoDaAtividade != null ?<p className="percurso">{p.atividade.percursoDaAtividade.nome}</p> : null}
                </ActivityCard>
        } else {
            const button =  <Button dark={false} label="inscrito" action="cancelar" handleClick={() => {handleCancelar(p.atividade.id)}} active={false}/>
            const buttonM = <Button dark={true} label="inscrito" action="cancelar" handleClick={() => {handleCancelar(p.atividade.id)}} active={false}/>
            return <ActivityCard details={<ActivityDetails button={buttonM} atividade={p.atividade} data={p.atividade.data} modal={"ver mais"}/>} key={p.id} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino} button={button}>
                <p className="title">{p.atividade.nome}</p>
                {p.atividade.percursoDaAtividade != null ? <p className="percurso">{p.atividade.percursoDaAtividade.nome}</p> : null}
            </ActivityCard>
        }
        
    }
    const rows = () => {
        let dias = props.atividadesDoUsuario.dias
        return dias.map(d =>
          { return <DayRow key={d.data} dia={d.dia} data={d.data} diaDaSemana={d.diaDaSemana} agenda={true}>
              { d.presencas.length > 0 ? d.presencas.map(presenca => {
                  return cardMaker(presenca)
        }) : <p className="messageInfo">Você não possui nenhuma atividade agendada neste dia</p>}
            </DayRow>
          })
    }
    return (
        <div>
            {!props.atividadesDoUsuario.loading && props.atividadesDoUsuario.errors == undefined ?  rows() : null}
        </div>
    );
}

export default _.flowRight(
    graphql(removePresencaFromUser,{name: "removePresenca"}),
    graphql(atividadesDoUsuario, {name: "atividadesDoUsuario",options: (props) => {return {variables: {usuarioId: JSON.parse(localStorage.getItem('user')).id, mes: props.mes + 1}}}})
)(index);
