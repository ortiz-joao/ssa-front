import React from 'react';
import ActivityDetails from '../activityDetails'
import ActivityCard from '../activityCardPattern'
import { graphql } from 'react-apollo';
import {atividadesDisponiveisDoAluno,atividadesMarcadas, addPresencaToUsuario} from '../../queries/queries'
import Button from '../button'
import _ from 'lodash'
const index = (props) => {
    const handleSub = (id) => {
        props.addPresenca({
            variables:{
                estado: 'pretendeAtender',
                papel: 'participante',
                atividadeId: parseInt(id),
                usuarioId: JSON.parse(localStorage.getItem('user')).id
            },
            refetchQueries: [{query: atividadesMarcadas},{query: atividadesDisponiveisDoAluno}]
        })
    }
    const atividadesDisponiveis= () => {
        const atividades = []
        props.atividadesDisponiveisDoAluno.usuario.aluno.percursosDoAluno.map(p => {return p.atividadesDoPercurso.map(a => {atividades.push(a)})})
        props.atividadesDisponiveisDoAluno.usuario.categoria.atividadesDisponiveis.map(a => {return atividades.push(a)})
        return atividades.map(atividade =>{
            if (atividade.participantes.filter(p => { return p.id == JSON.parse(localStorage.getItem('user')).id} ).length == 0) {
                const button = <Button dark={false} label="participar" action="participar" handleClick={() => {handleSub(atividade.id)}} active={false}/>
            const details = <ActivityDetails button={button} atividadeId={atividade.id} data={atividade.data} modal={"ver mais"}/>
            return (
            <ActivityCard key={atividade.id} details={details} inicio={atividade.horarioInicio} termino={atividade.horarioTermino} button={button}>
                    <p className="title">{atividade.nome}</p>
                    {atividade.percursoDaAtividade != null ? <p className="percurso">{atividade.percursoDaAtividade.nome}</p> : null}
                </ActivityCard>
            )
            }
            return null
        })
    }
    return (
        <div className="atividadesDisponiveis">
            {!props.atividadesDisponiveisDoAluno.loading && props.atividadesDisponiveisDoAluno.errors == undefined ? atividadesDisponiveis() : null}
        </div>
    );
}

export default _.flowRight(
    graphql(addPresencaToUsuario, {name: "addPresenca"}),
    graphql(atividadesDisponiveisDoAluno, {name: "atividadesDisponiveisDoAluno", options: (props) => {
        return { variables: {usuarioId: JSON.parse(localStorage.getItem('user')).id, data: props.data.split('/').reverse().join('/')}}
    }})
)(index);