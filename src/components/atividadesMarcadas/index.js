import React, { Component } from 'react';
import ActivityDetails from '../activityDetails'
import ActivityCard from '../activityCardPattern'
import { graphql } from 'react-apollo';
import {atividadesMarcadas} from '../../queries/queries'
class index extends Component {
    constructor(props){
        super(props)
    }
    atividades = () => {
        const presencas = this.props.data.presencas
        return presencas.map(p => {
            return <ActivityCard key={p.id} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino}>
                <p className="title">{p.atividade.nome}</p>
                {p.atividade.percursoDaAtividade != null ? <p className="percurso">{p.atividade.percursoDaAtividade.nome}</p> : null}
            <ActivityDetails atividade={p.atividade} data={p.atividade.data} modal={"ver mais"}/>
            </ActivityCard>
        })
    }
    render() {
        return (
            <div>
                {!this.props.data.loading && this.props.error == undefined ?  this.atividades() : null}
            </div>
        );
    }
}

export default graphql(atividadesMarcadas, {options: (props)=>{ return {variables:{usuarioId: props.usuarioId, data: props.data.split('/').reverse().join('/')}}}})(index);