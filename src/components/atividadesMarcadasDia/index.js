import React from 'react';
import ActivityDetails from '../activityDetails'
import ActivityCard from '../activityCardPattern'
import { graphql } from 'react-apollo';
import {atividadesMarcadas ,atividadesDisponiveisDoAluno, removePresencaFromUser} from '../../queries/queries'
import Button from '../button'
import _ from 'lodash'
const index = (props) => {
    const handleCancelar = (id) => {
        props.removePresenca({
            variables:{
                atividadeId: parseInt(id),
                usuarioId: JSON.parse(localStorage.getItem('user')).id
            },
            refetchQueries: [{query: atividadesMarcadas},{query: atividadesDisponiveisDoAluno}]
        })
    }
    const handleEditar = () => {
        
    }
    const atividadesMarcadass = () => {
        const presencas = props.atividadesMarcadas.presencas
        return presencas.map(p => {
            const button = p.papel != 'responsavel' ? <Button dark={false} label="inscrito" action="cancelar" handleClick={() => {handleCancelar(p.atividade.id)}} active={false}/> : <Button dark={false} label="editar" action="editar" handleClick={() => {handleEditar(p.atividade.id)}} active={false}/>
            const details = <ActivityDetails button={button} atividadeId={p.atividade.id} data={p.atividade.data} modal={"ver mais"}/>
            return (
            <ActivityCard key={p.atividade.id} details={details} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino} button={button}>
                    <p className="title">{p.atividade.nome}</p>
                    {p.atividade.percursoDaAtividade != null ? <p className="percurso">{p.atividade.percursoDaAtividade.nome}</p> : null}
            </ActivityCard>
            )
        })
    }
    return (
        <div className="atividadesMarcadas">
            {!props.atividadesMarcadas.loading && props.atividadesMarcadas.errors == undefined ? atividadesMarcadass() : null}
        </div>
    );
}

export default _.flowRight(
    graphql(removePresencaFromUser, {name: "removePresenca"}),
    graphql(atividadesMarcadas, {name:"atividadesMarcadas",options: (props)=>{ return {variables:{usuarioId: JSON.parse(localStorage.getItem('user')).id, data: props.data.split('/').reverse().join('/')}}}}))(index);