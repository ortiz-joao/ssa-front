import React from 'react';
import ActivityDetails from '../activityDetails'
import ActivityCard from '../activityCardPattern'
import Button from '../button'
import {atividadesRecentes} from '../../queries/queries'
import {graphql} from 'react-apollo'
const index = (props) => {
    const atividadesRecentes = () => {
        if (!props.data.loading) {
            const presencas = props.data.atividadesRecentes
        return presencas.map(p => {
            return <ActivityCard key={p.id} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino} button={<Button dark={false} label={p.estado} action={p.estado} active={false}/>}>
                <p className="title">{p.atividade.nome}</p>
                <p className="percurso">{p.atividade.percursoDaAtividade != null ? p.atividade.percursoDaAtividade.nome : null}</p>
            <ActivityDetails atividadeId={p.atividade.id} data={p.atividade.data} modal={"ver mais"}/>
            </ActivityCard>
        })
        }
        
    }
    return (
        <div className="atividadesRecentesContainer">
                {atividadesRecentes()}
            </div>
    );
}

export default graphql(atividadesRecentes,{options: (props)=>{ return {variables:{usuarioId: props.usuarioId}}}})(index);