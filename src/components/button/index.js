import React from 'react';
import './styles.css'
const index = (props) => {
    return (
        <div className={props.dark ? "dark buttonContainer" : "buttonContainer"}>
            <p className='fake'>{props.label}</p>
            <button className="button" onClick={props.handleClick}>
                <p className="buttonText">{props.action}</p>
            </button>
        </div>
    );
}

export default index;