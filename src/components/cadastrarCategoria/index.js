import React, { Component } from 'react';
import Input from '../inputField'
import Button from '../button'
import { graphql } from 'react-apollo';
import {  addCategoriaDeUsuario } from '../../queries/queries';

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categoria: null
        }
    }
    handleSubmit = (e) => {
        e.preventDefault()
        this.props.mutate(
            {variables: {
                categoria:this.state.categoria
            }}
        )
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    render() {
        return (
            <div className="container">
                <section>
                    <form className="formContainer">
                    <Input type="text" name="categoria" label="nome da categoria" handleChange={this.handleChange}/>
                    <Button dark={false} label="agendar" action="agendar" handleClick={this.handleSubmit} active={false}/>
                    </form> 
                </section>
            </div>
        );
    }
}

export default graphql(addCategoriaDeUsuario)(index);