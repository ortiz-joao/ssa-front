import React, { Component } from 'react';
import Input from '../inputField'
import Button from '../button'
import { graphql } from 'react-apollo';
import {  addDisciplina } from '../../queries/queries';

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            disciplina: null
        }
    }
    handleSubmit = (e) => {
        e.preventDefault()
        this.props.mutate(
            {variables: {
                disciplina : this.state.disciplina
            }}
        )
    }
    handleChange = (e) => {
        
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    render() {
        return (
            <div className="container">
                <section>
                    <form className="formContainer">
                    <Input type="text" name="disciplina" label="nome da disciplina" handleChange={this.handleChange}/>
                    <Button dark={false} label="criar" action="acriar" handleClick={this.handleSubmit} active={false}/>
                    </form> 
                </section>
            </div>
        );
    }
}

export default graphql(addDisciplina)(index);