import React, { Component } from 'react';
import AddActivities from '../addActivities'
import Input from '../inputField'
import Button from '../button'
import { graphql } from 'react-apollo';
import {addPercurso, addPercursoToProfessor} from '../../queries/queries'
import _ from 'lodash'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nome: null,
            limiteDeParticipantes: null,
            descricao: null,
            atividades: [],
            professor: JSON.parse(localStorage.getItem('user'))
        }
    }
    handleAtividade = (e) => {
        this.setState({
            atividades: e
        })
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        this.props.addPercurso(
            {
                variables: {
                    nome: this.state.nome,
                    limiteDeParticipantes: parseInt(this.state.limiteDeParticipantes),
                    descricao: this.state.descricao
                }
            }
        ).then(data => {
            console.log(data);
            const percurso = data.data.addPercurso.id
            this.props.addProfessor({
                variables: {
                    percursoId: percurso,
                    professorId: this.state.professor.professor.id
                }
            })
        })
    }
    render() {
        return (
            <div className="container">
                <section>
                    <p className="title uppercase">atividades do percurso</p>
                    <AddActivities handleChange={this.handleAtividades} atividades={this.state.atividades}/>
                </section>
                <section>
                    <form className="formContainer">
                    <Input type="text" name="nome" label="nome do percurso" handleChange={this.handleChange}/>
                    <Input type="text" name="limiteDeParticipantes" label="limite de participantes" handleChange={this.handleChange}/>
                    <Input type="text" name="descricao" label="descrição" handleChange={this.handleChange}/>
                    <Button dark={false} label="agendar" action="agendar" handleClick={this.handleSubmit} active={false}/>
                    </form> 
                </section>
            </div>
        );
    }
}

export default _.flowRight(
    graphql(addPercursoToProfessor, {name: "addProfessor"}),
    graphql(addPercurso, {name: "addPercurso"}))(index);