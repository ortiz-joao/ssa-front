import React, { Component } from 'react';
import Input from '../inputField'
import Button from '../button'
import RadioGroup from '../radioGroup'
import AddOptions from '../addOptions'
import AddGrupo from '../addGrupo'
import {graphql} from 'react-apollo'
import _ from 'lodash'
import { addUsuario, alunos, responsaveis, grupos, categoriasDeUsuarios, addAlunoToGrupo, addResponsavelToAluno, addProfessorToGrupo } from '../../queries/queries'
import Select from '../select'
import './styles.css'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tipo: null,
            nomeDeUsuario: null,
            nomeCompleto: null,
            email: null,
            turma: null,
            categoriaDeUsuarioId: null,
            senha: null,
            confirmacaoDaSenha: null,
            responsaveis: [],
            materias: null,
            responsavelPor: [],
            orientandos: [],
            grupos: []
        }
        this.props = props
    }
    handleSelect = (e) => {
        console.log(e.target.value);
        this.setState({
            categoriaDeUsuarioId: e.target.value
        })
    }
    handleGrupo = (e) => {
        this.setState({
            grupos: e
        })
    }
     handleChange = (e) => {
         this.setState({
             [e.target.id]: e.target.value
         })
     }
     handleSubmit = (e) => {
         e.preventDefault()
         const responsavelPor = this.state.responsavelPor
         this.props.addUsuario({
             variables: {
                 tipo: this.state.tipo,
                 nomeDeUsuario: this.state.nomeDeUsuario,
                 nomeCompleto: this.state.nomeCompleto,
                 email: this.state.email,
                 senha: this.state.senha,
                 categoriaDeUsuarioId: parseInt(this.state.categoriaDeUsuarioId),
                 turma: this.state.turma
             }
         }).then(e => {
             console.log(e.data.addUsuario.tipo);
             switch (e.data.addUsuario.tipo) {
                 case "professor":
                     if(this.state.grupos.length > 0){
                         this.state.grupos.map(g => {
                            this.props.addProfessorToGrupo({
                                variables:{
                                    professorId: parseInt(e.data.addUsuario.professor.id),
                                    grupoId: parseInt(g.id)
                               }
                            })
                         })
                     }
                     break;
                case "aluno":
                    if(this.state.responsaveis.length > 0){
                        this.state.responsaveis.map(r => {
                           return this.props.addResponsavelToAluno({
                               variables:{
                                    alunoId: parseInt(e.data.addUsuario.aluno.id),
                                    responsavelId: parseInt(r.id)
                              }
                           })
                        })
                    }
                    if(this.state.grupos.length > 0){
                        this.props.addAlunoToGrupo({
                            variables:{
                                alunoId: parseInt(e.data.addUsuario.aluno.id),
                                grupoId: parseInt(this.state.grupos[0].id)
                           }
                        })
                    }
                case "responsavel":
                        if(responsavelPor.length > 0){
                            responsavelPor.map(r => {
                                console.log(r);
                                return this.props.addResponsavelToAluno({
                                   variables:{
                                        alunoId: parseInt(r.id),
                                        responsavelId: parseInt(e.data.addUsuario.responsavel.id)
                                  }
                               })
                            })
                        }
                 default:
                     break;
             }
         })
     }
     handleRadio= (e) => {
         console.log(e)
        this.setState({
            tipo: e.target.value
        })
     }
     handleAlunos = (e) =>{
         this.setState({responsavelPor: e} 
         )
     }
     handleMaterias = (e) =>{
        this.setState({materias: e} 
        )
    }
    handleResponsaveis = (e) => {
        this.setState({
            responsaveis: e
        })
    }
     tipoOptions = () => {
         switch (this.state.tipo){
            case "aluno":
                 return (
                     <div>
                         <AddOptions handleSelect={this.handleResponsaveis} selectedOptions={this.state.responsaveis} options={this.props.responsaveis.loading ? [] : this.props.responsaveis.responsaveis}  title="responsaveis pelo aluno" optionId="id" optionLabel="nomeCompleto" />
                         <AddGrupo handleChange={this.handleGrupo} limite={1} title="grupo" optionId="id" optionLabel="grupo" />
                     </div>  
                 )
            case "professor":
                return (
                        <AddGrupo handleChange={this.handleGrupo} limite={5} title="grupo" optionId="id" optionLabel="grupo" />
                )
            case "responsavel":
                return <AddOptions handleSelect={this.handleAlunos} selectedOptions={this.state.responsavelPor} options={this.props.alunos.loading ? [] : this.props.alunos.alunos} title="responsavel por" optionId="id" optionLabel="nomeCompleto" />
         }
     }
    render() {
        return (
            <div className="cadastroContainer">
                <h2 className="pageTitle bigText">cadastro de usuario</h2>
                <section>
                    <p className="title uppercase">escolha o tipo de usuario
                    </p>
                        <RadioGroup options={[{name:"tipo", value: "aluno", label: "aluno"},{name:"tipo", value: "professor", label: "professor"},{name:"tipo", value: "responsavel", label: "responsavel"}]} handleSelect={this.handleRadio} />
                    {this.tipoOptions()}
                </section>
                <section>
                    <form className="loginContaienr" onSubmit={this.handleSubmit}>
                        <Input type="text" name="nomeDeUsuario" label="nome de usuario" handleChange={this.handleChange}/>
                        <Input type="text" name="nomeCompleto" label="nome completo" handleChange={this.handleChange}/>
                        <Input type="text" name="email" label="email" handleChange={this.handleChange}/>
                        <Select options={this.props.categorias.loading ? [] : this.props.categorias.categoriasDeUsuarios} handleChange={this.handleSelect}/>
                        {this.state.tipo == "aluno" ? <Input type="text" name="turma" label="turma" handleChange={this.handleChange}/> : null}
                        <Input type="password" name="senha" label="senha" handleChange={this.handleChange}/>
                        <Input type="password" name="confirmacaoDaSenha" label="confirme a senha" handleChange={this.handleChange}/>
                        <Button dark={false} label="cadastrar" action="cadastrar" handleClick={this.handleSubmit} active={false}/>
                    </form>
                </section>
            </div>
        );
    }
}

export default _.flowRight(
    graphql(addUsuario, {name: "addUsuario"}),
    graphql(categoriasDeUsuarios, {name: "categorias"}),
    graphql(responsaveis, {name: "responsaveis"}),
    graphql(alunos,{name: "alunos"}),
    graphql(grupos, {name: "grupos"}),
    graphql(addResponsavelToAluno, {name: "addResponsavelToAluno"}),
    graphql(addAlunoToGrupo, {name: "addAlunoToGrupo"}),
    graphql(addProfessorToGrupo, {name: "addProfessorToGrupo"})
)(index);