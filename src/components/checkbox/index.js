import React from 'react';

const index = (props) => {
    return (
        <div key={props.value} className="checkBoxContainer">
            <input onChange={props.onChange} checked={props.checked} className="checkbox" type="checkbox" id={props.name} name={props.name} value={props.value}/>
            <label className="checkboxLabel" htmlFor={props.name}>{props.label}</label>
        </div>
    );
}

export default index;