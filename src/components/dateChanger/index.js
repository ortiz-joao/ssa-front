import React, { Component } from 'react';
import './styles.css'
class index extends Component {
    constructor(props) {
        super(props)
        this.state={
            i: props.i
        }
    }
    handle = (i) => {
        this.props.handleChange(this.props.data[i])
    }
    next = () => {
        if (this.state.i != this.props.data.length - 1) {
            this.setState((prevState, props) => {
                this.handle(prevState.i + 1)
                return {i: prevState.i + 1}
            })
            
        } else {
            this.setState({
                i: 0
            })
            this.handle(0)
        }
        
    }
    back = () => {
        if (this.state.i > 0) {
            this.setState((prevState, props) => {
                this.handle(prevState.i - 1)
                return {i: prevState.i - 1}
            })
        } else {
            this.setState({
                i: this.props.data.length - 1
            })
            this.handle(this.props.data.length - 1)
        }
    }
    render() {
        return (
            <div className="dateChanger">
                <div onClick={this.back} className="arrowButton back">
                    a
                </div>
                <div className="data">
                    {this.props.data[this.state.i]}
                </div>
                <div onClick={this.next} className="arrowButton">
                    b
                </div>
            </div>
        )
    }
}

export default index;