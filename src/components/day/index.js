import React, { Component } from 'react';
import ActivityCard from '../activityCardPattern'
import Button from '../button'
import Input from '../inputField'
import ActivityDetails from '../activityDetails'
import DateChanger from '../dateChanger'
import AtividadesMarcadas from '../atividadesMarcadasDia'
import AtividadesDisponiveis from '../atividadesDisponiveis'
import './styles.css'
import moment from 'moment'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            search: null,
            dia: props.location.state.dia,
            data: props.location.state.dia,
            i: moment(props.location.state.dia, 'YYYY-MM-DD').date(),
            atividades: [{id:1,data: '2019-11-19' ,limiteDeParticipantes: 15,horarioDeInicio: "2019-11-19T12:35:00.000Z", horarioDeTermino: "2019-11-19T13:30:00.000Z", nome:"atividade", descricao: "atividade mt foda slc aaaaaaaaaaaaa lorem ip",presencas: [{estado:"presente",papel:"responsavel", usuario:{nomeCompleto:"Paulo"}}, {estado:"presente",papel:"participante", usuario:{nomeCompleto:"Paulo"}}], percurso:{nome:"percurso"}}]
        }
    }
    dias = () =>{
        const days = []
        const mes = moment(this.state.dia, 'YYYY-MM-DD').format('MM/YYYY')
        const diasNoMes = moment(mes , 'MM/YYYY').daysInMonth()
        for (let i = 0;i < diasNoMes; i++){
            let dia = moment("01/" + mes, "DD/MM/YYYY").add(i, 'd').format('DD[/]MM[/]YYYY')
            days.push(dia)
        }
        return days
    }
    handleDateChange = (d) => {
        const dia = moment(d, 'DD/MM/YYYY').format('DD/MM/YYYY')
        this.setState({
            data: dia 
        })
    }
    render() {
        return (
            <div className="dayContainer">
                <div className="header">
                    <Input type="text" label="pesquisar" name="search" handleChange={this.handleChange}/>
                    <DateChanger i={this.state.i - 1} handleChange={this.handleDateChange} data={this.dias()}/>
                    <span className="underlineButton" onClick={this.displayFilter}>filtrar</span>
                </div>
                <div className="atividadesMarcadasRows">
                    <span className="rowName uppercase"> atividades marcadas</span>
                    <AtividadesMarcadas data={this.state.data}/>
                </div>
                <div className="atividadesDisponiveisRow">
                <span className="rowName uppercase"> atividades disponiveis</span>
                <AtividadesDisponiveis data={this.state.data}/>
                </div>
            </div>
        );
    }
}

export default index;