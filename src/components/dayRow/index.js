import React from 'react';
import {Link} from 'react-router-dom'
import './styles.css'
const index = (props) => {
    const weekDay = (diaDaSemana) => {
        switch (diaDaSemana) {
            case 0:
                return "SEG"
                break;
            case 1:
                return "TER"
                break;
            case 2:
                return "QUA"
                break;
            case 3:
                return "QUI"
                break;
            case 4:
                return "SEX"
                break;
            case 5: 
                return "SAB"
                break;
            case 6:
                return "DOM"
                break;
        }
    }
    return (
        <div className="dayRowContainer">
            <div className="dayInfo">
                <h2>{props.dia}</h2>
                <span>{weekDay(props.diaDaSemana)}</span>
            </div>
            {props.children}
            {props.agenda ? <div className="seeDayContainer"><Link className="seeDay" to={{pathname: "/dia",state: {dia: props.data}}} >ver dia</Link></div> : null}
        </div>
    );
}

export default index;