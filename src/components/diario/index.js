import React, { Component } from 'react';
import Input from '../inputField'
import DateChanger from '../dateChanger'
import DayRow from '../dayRow'
import ActivityCard from '../activityCardPattern'
import Button from '../button'
import ActivityDetails from '../activityDetails'
import _ from 'lodash'
import {graphql} from 'react-apollo'
import {dias} from '../../queries/queries'
import moment from 'moment'
import PresencasDiario from '../presencasDiario'
class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: moment().format('MM[/]YYYY'),
            mes : moment().get('M'),
            search: null,
        };
    }
    months= () =>{
        let days = []
        for (let i = 0;i<=11;i++){
            let dia = moment("01/01/2019", "DD/MM/YYYY").add(i, 'M').format('MM[/]YYYY')
            days.push(dia)
        }
        return days
    }
    dias = (dias) => {
        for (let i = 0; i < dias.length; i++) {
            let j = i-1
            let tmp = dias[i].dia
            while ( j >= 0 && dias[j].dia > tmp) { // 9 > 0
                dias[j+1].dia = dias[j].dia
                j--
            }
            dias[j+1].dia = tmp
        }
        return dias
        
    }
    handleDateChange = (d) => {
        const mes = moment(d, 'MM-YYYY').get('M')
        this.setState({
            mes: mes
        })
    }
    rows = () => {
        
    }
    render() {
        return (
            <div className="container">
                <div className="header">
                    <Input type="text" name="search" handleChange={this.handleChange}/>
                    <DateChanger i={this.state.mes} handleChange={this.handleDateChange} data={this.months()}/>
                    <span className="underlineButton" onClick={this.displayFilter}>filtrar</span>
                </div>
                <div className="dias">
                    <PresencasDiario mes={this.state.mes}/>
                </div>
            </div>
        );
    }
}

export default _.flowRight(
    graphql(dias, {name: "dias"})
    )(index);