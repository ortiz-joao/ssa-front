import React from 'react';
import { graphql } from 'react-apollo';
import {percursos,percursosDoAluno,addPercursoToAluno} from '../../queries/queries'
import PercursoCard from '../percursoCardPattern'
import PercursoDetails from '../percursoDetails'
import Button from '../button'
import ActivityCard from '../activityCardPattern'
import ActivityDetails from '../activityDetails'
import _ from 'lodash'
const index = (props) => {
    const userId = JSON.parse(localStorage.getItem('user')).aluno.id
    const handleButton = () =>{

    }
    const atividades = (atividades,nome) => {
        return atividades.map(atividade => {
            const button = <Button dark={false} label="participar" action="participar" handleClick={ () => {handleButton(atividade.id)}} active={false}/>
            return <ActivityCard needDate={true} data={atividade.data} details={<ActivityDetails atividade={atividade} data={atividade.data} modal={"ver mais"}/>} key={atividade.id} inicio={atividade.horarioInicio} termino={atividade.horarioTermino} button={button}>
            <p className="title">{atividade.nome}</p>
            <p className="percurso">{nome}</p>
        </ActivityCard>
        })
    }
    const handleParticipar = (id) => {
        console.log(id);
        props.addPercurso({
            variables:{
                alunoId: parseInt(userId),
                percursoId: parseInt(id)
            },
            refetchQueries: [{query:percursos},{query:percursosDoAluno}]
        })
        
    }
    const percursosR = () => {
        return props.percursos.percursos.map(p => {
            if(p.alunos.filter(a => { return a.id == userId}).length == 0) {
                const button = <Button dark={false} label="participar" action="participar" handleClick={() => {handleParticipar(p.id)}} active={false}/>
                const details = <PercursoDetails left={<div><p className="title uppsercase">atividades</p>{atividades(p.atividadesDoPercurso,p.nome)}</div>} button={button} percurso={p}/>
                return <PercursoCard details={details} button={button}>
                    <p className="title">{p.nome}</p>
                    {p.professores.length > 0 ? <p className="infoText">{p.professores[0].usuario.nomeCompleto}</p> : null}
                </PercursoCard>
            } 
            return null
        })
    }
    return (
        <div>
            {!props.percursos.loading && props.percursos.errors == undefined ? percursosR() : null}
        </div>
    );
}

export default _.flowRight(graphql(percursos,{name: "percursos"}),graphql(addPercursoToAluno, {name:"addPercurso"}))(index);