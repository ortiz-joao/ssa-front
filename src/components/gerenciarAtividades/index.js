import React, { Component } from 'react';
import Tabs from '../tabs'

import CadastrarAtividade from '../cadastrarAtividade'


import _ from 'lodash'
import {dias , atividadesDoUsuario} from '../../queries/queries'
import {graphql} from 'react-apollo'

import MinhasAtividades from '../minhasAtividades'
import moment from 'moment'
import DateChanger from '../dateChanger'
    class index extends Component {
        constructor(props){
            super(props)
            this.state={
                tab: 0,
                data: moment().format('MM[/]YYYY'),
                mes : moment().get('M'),
                presencas: [
                    {estado: "presente",nota: 7,observacao: "aluno muito comportado", papel: "responsavel", atividade:{id:1,notaMaxima: 10,limiteDeParticipantes: 15,horarioDeInicio: "2019-11-19T12:35:00.000Z", horarioDeTermino: "2019-11-19T13:30:00.000Z", nome:"atividade", descricao: "atividade mt foda slc aaaaaaaaaaaaa lorem ip",presencas: [{estado:"presente",papel:"responsavel", usuario:{ id: 1,nomeCompleto:"Paulo"}}, {estado:"presente",papel:"participante", usuario:{ id: 1,nomeCompleto:"Paulo",categoria :{ categoria: "Aluno do 9 Ano"}}}], percurso:{nome:"percurso"}}},
                    {estado: "ausente",observacao: "aluno muito massa", papel: "responsavel", atividade:{id:2,limiteDeParticipantes: 15,horarioDeInicio: "2019-11-19T12:35:00.000Z", horarioDeTermino: "2019-11-19T13:30:00.000Z", nome:"atividade", descricao: "atividade mt foda slc aaaaaaaaaaaaa lorem ip",presencas: [{estado:"presente",papel:"responsavel", usuario:{nomeCompleto:"Paulo"}}, {estado:"presente",papel:"participante", usuario:{nomeCompleto:"Paulo"}}], percurso:{nome:"percurso"}}},
                    {estado: "ausente",observacao: "aluno muito comportado", papel: "responsavel", atividade:{id:3,limiteDeParticipantes: 15,horarioDeInicio: "2019-11-19T12:35:00.000Z", horarioDeTermino: "2019-11-19T13:30:00.000Z", nome:"atividade", descricao: "atividade mt foda slc aaaaaaaaaaaaa lorem ip", presencas: [{estado:"presente",papel:"responsavel", usuario:{nomeCompleto:"Paulo"}}, {estado:"presente",papel:"participante", usuario:{nomeCompleto:"Paulo"}}], percurso:{nome:"percurso"}}}
                ]
            }
        }
        months= () =>{
            let days = []
            for (let i = 0;i<=11;i++){
                let dia = moment("01/01/2019", "DD/MM/YYYY").add(i, 'M').format('MM[/]YYYY')
                days.push(dia)
            }
            return days
        }
        dias = (dias) => {
            for (let i = 0; i < dias.length; i++) {
                let j = i-1
                let tmp = dias[i].dia
                while ( j >= 0 && dias[j].dia > tmp) { // 9 > 0
                    dias[j+1].dia = dias[j].dia
                    j--
                }
                dias[j+1].dia = tmp
            }
            return dias
            
        }
        handleChange = (e) => {
            this.setState({tab : e})
        }
        criarAtividade = () => {
            return <CadastrarAtividade/>
        }
        handleEditar= (e) => {
            this.props.history.push({
                pathname: '/editarAtividade',
                search: '',
                state: { atividade: e }
              })
        }
        handleDateChange = (d) => {
            const mes = moment(d, 'MM-YYYY').get('M')
            this.setState({
                mes: mes
            })
        }
        minhasAtividades = () => {
            return <div><div className="headerDates"><DateChanger i={this.state.mes} handleChange={this.handleDateChange} data={this.months()}/></div>
                <MinhasAtividades handleEditar={this.handleEditar} mes={this.state.mes}/>
            </div>
            
        }
        render() {
            return (
                <div className="percursosContainer">
                    <Tabs handleChange={this.handleChange} initial={this.state.tab} options={[{value:0,label: "criar atividade"},{value: 1,label: "gerenciar atividades"}]}/>
                    {this.state.tab == 0 ? <div>{this.criarAtividade()}</div> : <div>{this.minhasAtividades()}</div>}
                </div>
            );
        }
    }
    
    export default index;