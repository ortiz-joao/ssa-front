import React from 'react';
import _ from 'lodash'
import {graphql} from 'react-apollo'
import { percursosDoProfessor } from '../../queries/queries';
import Button from '../button'
import PercursoCard from '../percursoCardPattern'
import PercursoDetails from '../percursoDetails'
import ActivityCard from '../activityCardPattern'
import ActivityDetails from '../activityDetails'
const user = JSON.parse(localStorage.getItem('user'))
const index = (props) => {
    const atividades = (atividades,nome) => {
        return atividades.map(atividade => {
            const button = <Button dark={false} label="editar" action="editar" handleClick={ () => {handleEditar(atividade.id)}} active={false}/>
            return <ActivityCard needDate={true} data={atividade.data} details={<ActivityDetails atividade={atividade} data={atividade.data} modal={"ver mais"}/>} key={atividade.id} inicio={atividade.horarioInicio} termino={atividade.horarioTermino} button={button}>
            <p className="title">{atividade.nome}</p>
            <p className="percurso">{nome}</p>
        </ActivityCard>
        })
    }
    const handleEditar= (i) => {
        return i
    }
    const handleClick= (i) => {
        return i
    }
    const renderPercursos = () => {
        return props.percursosDoProfessor.professor.percursos.map(p => {
            const button = <Button dark={false} label="editar" action="editar" handleClick={() => {handleClick(p.id)}} active={false}/>
            const details = <PercursoDetails percurso={p} left={<div><p className="title uppsercase">atividades</p>{atividades(p.atividadesDoPercurso,p.nome)}</div>} button={button} percurso={p}/>
            return <PercursoCard details={details} button={button}>
                <p className="title">{p.nome}</p>
                {/* <p className="infoText">{p.professores[0].usuario.nomeCompleto}</p> */}
            </PercursoCard>
        })
    }
    return (
        !props.percursosDoProfessor.loading && props.percursosDoProfessor.errors == undefined ? renderPercursos() : null
    );
}

export default _.flowRight(
    graphql(percursosDoProfessor, {variables:{ professorId: user.professor ? user.professor.id : 0},name: "percursosDoProfessor"}))(index);