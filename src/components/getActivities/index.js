import React, { Component } from 'react';
import Card from '../activityCard';
import Button from '../button'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: props.selectedActivities
        }
    }
    options = (options) => {
        return options.map(o => {
            if (this.state.selected.filter(id => {return id == o.id}).length > 0){
                return (
                    <Card added={true} atividade={o} handleChange={this.handleChange} />
                )
            } else {
                return (
                    <Card added={false} atividade={o} handleChange={this.handleChange} />
                )
            }
            
        })
    }
    handleChange = (e) =>{
        console.log(e);
        let s = this.state.selected
        console.log(s);
        if (s.filter(o => {return o.id == e.id}).length > 0) {
            let selected = s.filter(o => {
                return o.id != e.id
            })
            this.setState({
                selected: selected
            })
        } else {
            let selected = [...s,e]
            this.setState({
                selected: selected
            })
        }
    }
    handleSubmit = () => {
        this.props.handleChange(this.state.selected)
        this.props.show()
    }
    render() {
        return (
            <div className="optionsContainer">
                {this.options([{id: 1,horarioDeInicio: '2038-01-19 03:14:07',notaMaxima: 10 ,horarioDeTermino: '2038-01-19 03:14:07', nome: 'medaaaaaa', tipo: 'palestra',materia: {disciplina: 'artes'}}])}
                <Button dark={false} label="adicionar" action="adicionar" handleClick={this.handleSubmit} active={false} />
            </div>
        );
    }
}

export default index;