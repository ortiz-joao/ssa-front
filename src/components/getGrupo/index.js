import React, { Component } from 'react';
import Checkbox from '../checkbox'
import Button from '../button'
import chalk from 'chalk';

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedOptions: props.selectedOptions
        }
    }
    options = (options) => {
        return options.map(o => {
            if (this.props.selectedOptions.filter(option => {return option.id == o.id}).length > 0){
                return (
                    <div className="grupoCheck" key={o.id}>
                        <p className="title uppercase">grupo</p>
                        {this.integrantes(o.integrantes)}
                    <Checkbox checked={true} onChange={(e) => this.handleChange(e,o)} label="selecionar" name={o.id} value={o.id}/>
                    </div>
                )
            } else {
                return (
                    <div className="grupoCheck" key={o.id}>
                        <p className="title uppercase">grupo</p>
                        {this.integrantes(o.integrantes)}
                    <Checkbox onChange={(e) => this.handleChange(e,o)} label="selecionar" name={o.id} value={o.id}/>
                    </div>
                )
            }
        })
    }
    integrantes = (integrantes) => {
        return integrantes.map(i => {
                return <p key={i.usuario.id} className="infoText">{i.usuario.nomeCompleto + ' - ' + i.turma}</p>
        })
    }
    handleChange = (e,g) =>{
        console.log(e.target.checked);
        let s = this.state.selectedOptions
        if (s.filter(o => {return o.id == g.id}).length > 0) {
            let selected = s.filter(o => {
                return o.id != g.id
            })
            this.setState({
                selectedOptions: selected
            })
        } else {
            let selected = [...s,g]
            this.setState({
                selectedOptions: selected
            })
        }
    }
    handleSubmit = () => {
        this.props.handleSelect(this.state.selectedOptions)
    }
    render() {
        return (
            <div className="optionsContainer">
            {this.options([{id: 1, integrantes: [{turma: "6B",usuario:{id: 1, nomeCompleto:"paulim da silva"}}]}])}
            <Button dark={false} label="adicionar" action="adicionar" handleClick={this.handleSubmit} active={false}/>
        </div>
        );
    }
}

export default index;
