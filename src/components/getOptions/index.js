import React, { Component } from 'react';
import Checkbox from '../checkbox'
import Button from '../button'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: props.selectedOptions
        }
    }
    options = (options) => {
        return options.map(o => {
            if (this.state.selected.filter(option => {return option.id == o.id}).length > 0){
                return (
                    <div key={o.id}>
                    {o.usuario != null ? <Checkbox checked={true} onChange={(value) => this.handleChange(value)} label={o.usuario.nomeCompleto} name={o.usuario.nomeCompleto} value={o.id}/> : <Checkbox checked={true} onChange={(value) => this.handleChange(value)} label={o.nome} name={o.nome} value={o.id}/>}
                    </div>
                )
            } else {
                return (
                    <div key={o.id}>
                    {o.usuario != null ?<Checkbox checked={false} onChange={(value) => this.handleChange(value)} label={o.usuario.nomeCompleto} name={o.usuario.nomeCompleto} value={o.id}/> : <Checkbox checked={false} onChange={(value) => this.handleChange(value)} label={o.nome} name={o.nome} value={o.id}/>}
                    </div>
                )
            }
            
        })
    }
    handleChange = (e) =>{
        let s = this.state.selected
        if (s.filter(o => {return o.id == e.target.value}).length > 0) {
            let selected = s.filter(o => {
                return o.id != e.target.value
            })
            this.setState({
                selected: selected
            })
        } else {
            let selected = [...s,{nomeCompleto: e.target.name,id: e.target.value}]
            this.setState({
                selected: selected
            })
        }
    }
    handleSubmit = () => {
        this.props.handleSelect(this.state.selected)
        this.props.show()
    }
    render() {
        return (
            <div className="optionsContainer">
                {this.options(this.props.options)}
                <Button dark={false} label="adicionar" action="adicionar" handleClick={this.handleSubmit} active={false} />
            </div>
        );
    }
}

export default index;