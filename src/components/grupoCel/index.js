import React from 'react';
import Button from '../button'
const index = (props) => {
    const integrantes = (integrantes) => {
        return integrantes.map(i => {
               return <p key={i.usuario.id} className="infoText">{i.usuario.nomeCompleto + ' - ' + i.turma}</p>
        })
    }
    const options = () => {
        let options = props.selected
        return ( options.map(o => {
            return (<div key={o.id} className="grupoCel">
                <span className="underlineButton remove" onClick={() => {props.removeOpcao(o.id)}}>remover</span>
                <p className="title uppercase">Grupo</p>
                {integrantes(o.integrantes)}
            </div>)
        })
        )
    }
    return (
        <div>
            {options()}
        </div>
    );
}

export default index;