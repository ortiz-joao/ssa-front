import React from 'react';

const index = (props) => {
    return (
       <div className="header">
           {props.children}
       </div> 
    );
}

export default index;