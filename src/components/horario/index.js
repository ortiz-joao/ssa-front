import React, { Component } from 'react';
import MaskedInput from 'react-text-mask'
import './styles.css'
class index extends Component {
    constructor(props){
        super(props)
            this.state = {
                horario: props.horario,
                dia: props.dia,
                editable: false
            }
        }
    handleChange = (event) => {
        this.setState({[event.target.id]: event.target.value});
      }
    editarHorario = () =>{
        this.setState((prevState, props) => {return {editable: !prevState.editable}})
        this.props.editarHorario({horario:this.props.horario,dia: this.props.dia},{horario:this.state.horario,dia: this.state.dia})
      }
    render() {
        return (
            <div className="horarioEditable">
                    <MaskedInput mask={[/\d/,/\d/,':',/\d/,/\d/]} className={this.state.editable ? "editable horarioText" : "horarioText"} type="text"  id="horario" value={this.state.horario} onChange={this.handleChange}/>
                    <MaskedInput mask={[/\d/,/\d/,'/',/\d/,/\d/]} className={this.state.editable ? "editable horarioText" : "horarioText"} type="text"  id="dia" value={this.state.dia} onChange={this.handleChange}/>
                    <div className="actions">
                        {this.state.editable ? <div className="action" onClick={this.editarHorario}>salvar</div> :<div className="action" onClick={() => {this.setState((prevState, props) => {return {editable: !prevState.editable}})}}>editar</div>}
                        <div className="action" onClick={() => {this.props.removerHorario({horario : this.state.horario, dia: this.state.dia})}}>remover</div>
                    </div>
                </div>
        )
    }
}

export default index;