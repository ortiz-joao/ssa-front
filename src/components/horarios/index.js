import React, { Component } from 'react';
import Horario from '../horario'
import AddHorario from '../addHorario'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
    }
    displayHorarios = (horarios) => {
        return horarios.map(h => {

            return (
                <Horario removerHorario={this.props.removerHorario} editarHorario={this.props.editarHorario} key={h.horario} horario={h.horario} dia={h.dia}/>
            )
        })
    }
    show = () =>{
        this.setState((prevState, props) => {
            return {show : !prevState.show}
        })
    }
    addHorario = (e) => {
        this.props.addHorario(e)
        this.setState({
            show: false
        })
    }
    render() {
        return (
            <div className="horarios">
            {this.displayHorarios(this.props.horarios)}
            <p onClick={this.show}>adicionar</p>
            {this.state.show ? <AddHorario addHorario={this.addHorario}/> : null}
        </div>
        );
    }
}

export default index;