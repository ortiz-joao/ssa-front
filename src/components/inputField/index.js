import React from 'react';
import './styles.css'
const index = (props) => {
    return (
        <div className="inputContainer">
            <input 
                placeholder="a"
                minLength="1"
                type={props.type}
                className="input"
                id={props.name}
                onChange={props.handleChange}
            />
            <label htmlFor={props.name} className="inputLabel">{props.label}</label>
        </div>
    );
}

export default index;