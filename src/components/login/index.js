import React, { Component } from 'react';
import Input from '../inputField'
import Button from '../button'
import {graphql} from 'react-apollo'
import {userLogin, currentUser} from '../../queries/queries'
import './styles.css'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            username: null,
            password: null,
            valid: false
        }
        this.props = props
    }
     handleChange = (e) => {
         this.setState({
             [e.target.id]: e.target.value
         })
     }
     handleSubmit = (e) => {
         e.preventDefault()
         const user = this.state
         this.props.mutate({variables:{username: user.username, password: user.password}}).then(data=> {
             console.log(data.data.login);
             if(data.data.login != undefined){
                 this.props.doLogin(data.data.login)
             }
         })
     }
    render() {
        return (
            <div className="loginContainer">
                <form onMouseLeave={() => {this.setState({valid: false})}} onMouseOver={() => {this.setState({valid: true})}} onSubmit={this.handleSubmit}>
                <h2 className="bigText loginTitle">LOGIN</h2>
                <Input type="text" label="nome de usuario" name="username" handleChange={this.handleChange}/>
                <Input type="password" label="senha" name="password" handleChange={this.handleChange}/>
                <Button dark={false} label="entrar" action="entrar" handleClick={this.handleSubmit} active={this.state.valid}/>
            </form>
            </div>
            
        );
    }
}

export default graphql(userLogin)(index);