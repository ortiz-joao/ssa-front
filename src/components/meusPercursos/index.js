import React from 'react';
import { graphql } from 'react-apollo';
import {percursosDoAluno, percursos, removePercursoFromAluno} from '../../queries/queries'
import PercursoCard from '../percursoCardPattern'
import PercursoDetails from '../percursoDetails'
import Button from '../button'
import ActivityCard from '../activityCardPattern'
import ActivityDetails from '../activityDetails'
import _ from 'lodash'
const user = JSON.parse(localStorage.getItem('user'))
const index = (props) => {
    const userId = JSON.parse(localStorage.getItem('user')).aluno.id
    const handleButton = () =>{

    }
    const atividades = (atividades,nome) => {
        return atividades.map(atividade => {
            const button = <Button dark={false} label="participar" action="participar" handleClick={ () => {handleButton(atividade.id)}} active={false}/>
            return <ActivityCard needDate={true} data={atividade.data} details={<ActivityDetails atividade={atividade} data={atividade.data} modal={"ver mais"}/>} key={atividade.id} inicio={atividade.horarioInicio} termino={atividade.horarioTermino} button={button}>
            <p className="title">{atividade.nome}</p>
            <p className="percurso">{nome}</p>
        </ActivityCard>
        })
    }
    const handleRemover = (id) => {
        props.removePercursoFromAluno({
            variables:{alunoId: parseInt(userId),
            percursoId: parseInt(id)},
            refetchQueries: [{query:percursos},{query:percursosDoAluno}],
        })
    }
    const renderPercursos = () => {
        return props.percursos.aluno.percursosDoAluno.map(p => {
            const button = <Button dark={false} label="Inscrito" action="cancelar" handleClick={() => {handleRemover(p.id)}} active={false}/>
            const details = <PercursoDetails left={<div><p className="title uppsercase">atividades</p>{atividades(p.atividadesDoPercurso,p.nome)}</div>} button={button} percurso={p}/>
            return <PercursoCard details={details} button={button}>
                <p className="title">{p.nome}</p>
                {/* <p className="infoText">{p.professores[0].usuario.nomeCompleto}</p> */}
            </PercursoCard>
        })
    }
    return (
        <div>
            {!props.percursos.loading && props.percursos.errors == undefined ? renderPercursos() : null}
        </div>
    );
}

export default _.flowRight(
    graphql(removePercursoFromAluno, {name: "removePercursoFromAluno"}),
    graphql(percursosDoAluno,{name: "percursos", variables: {id: user.aluno ? user.aluno.id : 0}}),)(index);