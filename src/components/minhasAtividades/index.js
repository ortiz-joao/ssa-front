import React from 'react';
import _ from 'lodash'
import {graphql} from 'react-apollo'
import {atividadesDoUsuario} from '../../queries/queries'
import ParticipanteCard from '../participanteCard'
import ActivityCard from '../activityCardPattern'
import ActivityDetails from '../activityDetails'
import Button from '../button'
import DayRow from '../dayRow'
const index = (props) => {
    const participantes = (atividade) => {
        if (atividade.presencas.length > 0) {
            return atividade.presencas.map(p => {
                if(p.papel != "responsavel") {
                    if(p.estado == "presente") {
                        return <ParticipanteCard options={[{name:"presenca", checked: true, value: "presente", label: "presente"},{name:"presenca", value: "ausente", label: "ausente"}] } usuario={p.usuario} atividadeId={atividade.id}/> 
                    }
                } else {
                    return null
                }
                
        })} else {
            return null
        }
    }
    const handleEditar = (e) => {
        props.handleEditar(e)
    }
    const minhasAtividades = () => {
        const dias = props.atividadesDoUsuario.dias
        return dias.map( d => {return d.presencas.length > 0 ? <DayRow key={d.data} dia={d.dia} diaDaSemana={d.diaDaSemana}>
        {d.presencas.map(p => {
        const part = participantes(p.atividade)
        const button = <Button dark={false} label="editar" action="editar" handleClick={() => {handleEditar(p.atividade)}} active={false}/>
        const leftInfo = <div>
        {<div><p className="title uppercase">limite de participantes</p><p className="uppercase infoItem">{p.atividade.limiteDeParticipantes + " vagas"}</p></div>}
        {p.nota != null ? <div><p className="title uppercase">nota</p><p className="infoTextItem">Peso da avaliação:{" "+p.atividade.notaMaxima}</p></div>: null}
        {<div><p className="title uppercase">descricao</p><p className="infoItem infoText">{p.atividade.descricao}</p></div>}
        </div>
        return <ActivityCard details={<ActivityDetails right={part} manage={true} left={leftInfo} atividade={p.atividade} data={d.data} modal={"ver mais"}/>} key={p.id} inicio={p.atividade.horarioInicio} termino={p.atividade.horarioTermino} button={button}>
            <p className="title">{p.atividade.nome}</p>
            {p.atividade.percursoDaAtividade != null ?<p className="percurso">{p.atividade.percursoDaAtividade.nome}</p> : null}
        </ActivityCard>
        })}
        </DayRow> : null})
    }
    return (
        <div className="dias">
            {!props.atividadesDoUsuario.loading && props.atividadesDoUsuario.error == undefined ? minhasAtividades() : null}
        </div>
    );
}

export default _.flowRight(
    graphql(atividadesDoUsuario, {name: "atividadesDoUsuario",options: (props) => {return {variables: {papel: 'responsavel',usuarioId: JSON.parse(localStorage.getItem('user')).id, mes: props.mes + 1}}}})
)(index);