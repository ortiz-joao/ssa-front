import React from 'react';
import { NavLink } from 'react-router-dom'
import './styles.css'
const index = (props) => {
    return (
     <nav className="navigation">
         <div className="logo">logo</div>
         <ul className="links">
            <li className="linkButton"><NavLink exact to="/">agenda</NavLink></li>
            <li className="linkButton"><NavLink to="/perfil">perfil</NavLink></li>
            <li className="linkButton"><NavLink to="/diario">diario</NavLink></li>
            <li className="linkButton"><NavLink to="/percursos">percursos</NavLink></li>
            <li className="linkButton"><NavLink to="/gerenciarAtividades">gerenciar atividades</NavLink></li>
         </ul>
         <div className="singout" onClick={props.logout}>sair</div>
     </nav>  
    )
}

export default index;