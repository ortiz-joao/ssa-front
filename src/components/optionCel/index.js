import React from 'react';

const index = (props) => {
    const Options = (props) => {
        if (props.selectedOptions.length > 0) {
            return props.selectedOptions.map(r => {
                return (
                <div key={r[props.id]} className="option">
                    <div className="label">{r[props.label]}</div>
                    <span onClick={() => {props.removeOpcao(r[props.id])}} className="underlineButton">remover</span>
                </div>
                )
            })
        } else {
            return null
        }
    }
    return (
      <div>
          {Options(props)}
      </div>  
    );
}

export default index;