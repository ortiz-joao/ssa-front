import React from 'react';
import RadioGroup from '../radioGroup'
import PresenceModal from '../presenceModal'
import _ from 'lodash'
import {graphql} from 'react-apollo'
import {updatePresenca } from '../../queries/queries'
const index = (props) => {
    const handlePresenca = (e) => {
        console.log(props.atividadeId);
        console.log(props.usuario.id);
        console.log(e.target.value);
        props.update({
            variables:{
                usuarioId: parseInt(props.usuario.id),
                atividadeId: parseInt(props.atividadeId),
                estado: e.target.value
            }
        })
    }
    return (
        <div className="participanteContainer">
            <div className="left">
                <p>{props.usuario.nomeCompleto}</p>
                <PresenceModal usuario={props.usuario} atividadeId={props.atividadeId}/>
            </div>
            <div className="right">
            <RadioGroup options={props.options} handleSelect={handlePresenca} />
            </div>
        </div>
    );
}

export default graphql(updatePresenca, {name: "update"})(index);