import React from 'react';

const index = (props) => {
    return (
        <div>
            <div className="button">
                {props.button}
            </div>
            <div className="info">
                {props.children}
            </div>
            <div className="bottomRigth">
                {props.details}
            </div>
        </div>
    );
}

export default index;