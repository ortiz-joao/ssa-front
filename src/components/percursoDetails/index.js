import React, { Component } from 'react';
import {graphql} from 'react-apollo'
import { percurso } from '../../queries/queries';
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
        this.props = props
    }
    countVagas(limite, presencas){
        console.log(presencas.length);
        return limite - (presencas.length) - 1
    }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
     materias = (disciplinas) => {
         return disciplinas.map(d => {
             return d.disciplina
         })
     }
    horario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
    responsavel = (presencas) => {
        return presencas.filter(p => {
            return p.papel == "responsavel"
        })
    }
    render() {
        return (
            <div className="container">
                {this.state.show ? (
                    <div className="modal">
                        <section className="firstSection">
                    <p className="title">{this.props.data.percurso.nome}</p>
                    <p className="title capitalized">{"professor - " + this.props.data.percurso.professores[0].usuario.nomeCompleto}</p>
                    <p className="title uppercase">percurso</p>
                    {this.props.left}
                    {this.props.button}
                </section>
                <section className="secondSection">
                    <button onClick={() => {this.setState((prevState, props) =>{return {show: !prevState.show}})}}>x</button>
                    <p className="title uppercase">vagas</p>
                    <p className="infoText">{this.countVagas(this.props.data.percurso.limiteDeParticipantes, this.props.data.percurso.alunos)}</p>
                    <p className="title ippercase">descrição</p>
                    <p className="textInfo">{this.props.data.percurso.descricao}</p>
                    {this.props.right}
                </section>
                    </div>
                ) : 
                <button className="underlineButton" onClick={() => {this.setState((prevState, props) =>{return {show: !prevState.show}})}}>ver mais</button>}
            </div> 
        );
    }
}

export default graphql(percurso,{options: (props) => { return {variables: {id: props.percurso.id}}}})(index);