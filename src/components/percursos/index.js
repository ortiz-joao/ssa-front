import React, { Component } from 'react';
import Tabs from '../tabs'
import PercursoCard from '../percursoCardPattern'
import PercursoDetails from '../percursoDetails'
import Button from '../button'
import ActivityCard from '../activityCardPattern'
import ActivityDetails from '../activityDetails'
import EncontrarPercursos from '../encontrarPercursos'
import MeusPercursos from '../meusPercursos'
import CriarPercurso from '../cadastrarPercurso'
import GerenciarPercursos from '../gerenciarPercursos'
    class index extends Component {
        constructor(props){
            super(props)
            this.state={
                tab: 0,
                user: JSON.parse(localStorage.getItem('user'))
            }
        }
        handleChange = (e) => {
            this.setState({tab : e})
        }
        atividades = (atividades) => {
            return atividades.map(atividade => {
                const button = <Button dark={false} label="participar" action="participar" handleClick={this.handleCancelar} active={false}/>
                return <ActivityCard needDate={true} data={atividade.data} details={<ActivityDetails atividade={atividade} data={atividade.data} modal={"ver mais"}/>} key={atividade.id} inicio={atividade.horarioDeInicio} termino={atividade.horarioDeTermino} button={button}>
                <p className="title">{atividade.nome}</p>
                <p className="percurso">{atividade.percurso.nome}</p>
            </ActivityCard>
            })
        }
        encontrarPercursos = () => {
           return <EncontrarPercursos />
        }
        meusPercursos = () => {
            return <MeusPercursos />
        }
        CriarPercurso = () => {
            return <CriarPercurso />
        }
        gerenciarPercursos = () => {
            return <GerenciarPercursos />
        }
        percursos = () =>{
            if (this.state.user.tipo == "professor") {
                return <div className="percursosContainer">
                <Tabs handleChange={this.handleChange} initial={this.state.tab} options={[{value:0,label: "criar percurso"},{value: 1,label: "gerenciar percursos"}]}/>
                {this.state.tab == 0 ? <div>{this.CriarPercurso()}</div> : <div>{this.gerenciarPercursos()}</div>}
                </div>
            }
            return <div className="percursosContainer">
            <Tabs handleChange={this.handleChange} initial={this.state.tab} options={[{value:0,label: "meus percursos"},{value: 1,label: "encontrar percursos"}]}/>
            {this.state.tab == 0 ? <div>{this.meusPercursos()}</div> : <div>{this.encontrarPercursos()}</div>}
            </div>
        }
        render() {
            return (
                this.percursos()
            );
        }
    }
    
    export default index;