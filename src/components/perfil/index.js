import React, { Component } from 'react';
import AtividadesMarcadas from '../atividadesMarcadas'
import AtividadesRecentes from '../atividadesRecentes'
import DateChanger from '../dateChanger'
import moment from 'moment'
import _ from 'lodash'
import './styles.css'
class index extends Component {
    constructor(props) {
        super(props)
        this.state= {
            user: JSON.parse(localStorage.getItem('user')),
            dia: 0,
            data: moment().format('DD[/]MM[/]YYYY')
        }
    }
    handleDateChange = (d) => {
        this.setState({
            data: d
        })
    }
    nextDays= () =>{
        let days = []
        for (let i = 0;i<=3;i++){
            let dia = moment().add(i, 'd').format('DD[/]MM[/]YYYY')
            console.log(dia);
            days.push(dia)
        }
        return days
    }
    render() {
        return (
            <div className="profileContainer">
                <section className="profileInfo">
                <p className="title uppercase">{this.state.user.nomeDeUsuario}</p>
                <p className="info">{this.state.user.nomeCompleto}</p>
                <p className="info uppercase">{"aluno - " + this.state.user.categoria.categoria}</p>
                <p className="info">{this.state.user.email}</p>
                <p className="title uppercase">responsaveis</p>
                <p className="itemInfo"></p>
                <p className="title">presença</p>
                <p className="title">boletim</p>
                </section>
                <section>
                    <div className="atividadesMarcadasRow">
                        <DateChanger i={this.state.dia} handleChange={this.handleDateChange} data={this.nextDays()}/>
                        <AtividadesMarcadas data={this.state.data} usuarioId={this.state.user.id}/>
                    </div>
                    <AtividadesRecentes usuarioId={this.state.user.id}/>
                </section>
                
            </div>
        );
    }
}

export default index;