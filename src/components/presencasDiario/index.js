import React from 'react';
import DayRow from '../dayRow'
import ActivityCard from '../activityCardPattern'
import Button from '../button'
import ActivityDetails from '../activityDetails'
import _ from 'lodash'
import {graphql} from 'react-apollo'
import {diario} from '../../queries/queries'

const index = (props) => {
    const diario = () => {
        let dias = props.data.dias
        return dias.map(d => {
            console.log(d)
           return d.diario.length > 0 ? <DayRow key={d.data} dia={d.dia} diaDaSemana={d.diaDaSemana}>
        {d.diario.map(p => {
            const button = <Button dark={false} label={p.estado} action={p.estado} active={false}/>
        const leftInfo = <div>
            {p.observacao != null ? <div><p className="title uppercase">observações</p><p className="infoText">{p.observacao}</p></div> : null}
            {p.nota != null ? <div><p className="title uppercase">nota</p><p className="infoTextItem">Peso da avaliação:{" "+p.atividade.notaMaxima}</p><p className="infoTextItem">Sua nota:{" "+p.nota}</p></div>: null}
            {button}
        </div>
            const presencaInfo = <div><p className="title">Observações</p><p className="infoText">{p.observacao}</p></div>
            return <ActivityCard details={<ActivityDetails left={leftInfo} atividade={p.atividade} data={d.data} modal={"ver mais"}/>} key={p.id} inicio={p.atividade.horarioInicio} presencaInfo={presencaInfo} termino={p.atividade.horarioTermino} button={button}>
                <p className="title">{p.atividade.nome}</p>
                {p.atividade.percursoDaAtividade != null ? <p className="percurso">{p.atividade.percursoDaAtividade.nome}</p>: null}
            </ActivityCard>
        })}
            </DayRow> : null
        })
        
    }
    return (
        <div className="container">
            {!props.data.loading && props.data.error == undefined ? diario() : null}
        </div>
    );
}

export default _.flowRight(
    graphql(diario, {options: (props) => {return { variables: {id:JSON.parse(localStorage.getItem('user')).id,mes: props.mes + 1}}}})
)(index);