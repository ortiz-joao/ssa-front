import React, { Component } from 'react';
import Button from '../button'
import ActivityDetails from '../presenceActivityDetails'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showDetails: false,
            grades: null,
            buttonLabel: null
        }
        this.props = props
    }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
     cardHorario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
     pastActivity() {
        const d = new Date(this.props.presenca.atividade.horarioDeTermino)
        const today = new Date()
        return today.getTime() < d.getTime()
     }
     grades = () => {
         if (((this.props.presenca.atividade.notaMaxima * 60) / 100) <= this.presenca.nota){
            return 'aprovado'
             
         }
        return 'reprovado'
       
     }
     handleUnsubscribe = () => {
        
     }
     seeActivity = () => {
        this.setState((prevState, props) => {
            return {showDetails: !prevState.showDetails}
        })
     }
    render() {
        return (
            <div className="cardContainer">
                {this.pastActivity() ? <Button dark={false} label="inscrito" action="cancelar" handleClick={this.handleUnsubscribe} active={false}/> :
                this.props.presenca.atividade.tipo != 'avaliacao' ? <Button dark={false} label={this.props.presenca.estado} action={this.props.presenca.estado} handleClick={this.seeActivity} active={false}/> : <Button dark={false} label={this.state.grades} handleClick={this.seeActivity} active={false}/>}
                <div className="cardInfo">
                    <div className="horario"> {this.cardHorario(this.props.presenca.atividade.horarioDeInicio, this.props.presenca.atividade.horarioDeTermino)}</div>
                    <div className="nomeDaAtividade">{this.props.presenca.atividade.nome}</div>
                    <div className="tipo-materia">{this.props.presenca.atividade.tipo + ' - ' + this.props.presenca.atividade.materia.disciplina}</div>
                    {this.props.presenca.atividade.tipo == 'avaliacao' ? 
                    (<p className="infoDescription">{'NOTA' + this.props.presenca.nota}</p>) : null}
                </div>
        {this.props.diario ? <div className="cardObservation"><p className="title">Observação</p><p className="textInfo">{this.props.presenca.observacao}</p></div> : null}
                <span className="seeActivityDetails" onClick={this.seeActivity}>ver mais</span>
                {this.state.showDetails ? <ActivityDetails closeView={this.seeActivity} atividade={{data: '2019-08-09',horarioDeInicio: '2019-08-09 10:53:00',horarioDeTermino: '2019-08-09 10:53:00',tipo: "avaliacao", materia: ['artes'],
            nome: 'uabuebau', presencas: ['upga', 'gubdau', 'huehe'], maximoDeParticipantes: 10, local: 'local',percurso: {nome:'percurso', id:1}
            ,notaMaxima: 10,descricao: "uashiuehsauihuahuahiahshasiuashaisuahsaseushiasuhasiuhsiu"}} presenca={{nota: 7, observacao: 'tu meda o popo'}} buttonLabel={this.pastActivity() ? 'inscrito' : this.props.presenca.atividade.tipo == 'avaliacao' ? this.grades() : this.props.presenca.estado} buttonAction={this.pastActivity() ? 'cancelar' : null} handleClick={this.pastActivity() ? null : this.handleUnsubscribe}/> : null}
            </div>
        );
    }
}

export default index;