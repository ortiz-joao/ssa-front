import React, { Component } from 'react';
import Button from '../button'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            responsavel: null
        }
        this.props = props
    }
    countVagas(limite, presencas){
        return (presencas.lenght - 1) - limite
    }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
     materias = (disciplinas) => {
         return disciplinas.map(d => {
             return d.disciplina
         })
     }
    horario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
     gotToPercurso = (id) => { 

     }
    render() {
        return (
            <div className="container">
                <span onClick={this.props.closeView}>X</span>
                <section className="firstSection">
                    <p className="title uppercase">{this.props.atividade.data.replace('-', '.') + ' ' + this.horario(this.props.atividade.horarioDeInicio, this.props.atividade.horarioDeTermino)}</p>
                    <p className="title">{this.props.atividade.tipo + ' - ' + this.materias(this.props.atividade.materia)}</p>
                    <p className="title">{this.props.atividade.nome}</p>
                    {/* <p className="subTitle">{this.props.atividade.presencas[1] + ' - ' + this.state.responsavel.nomeCompleto}</p> */}
                    <p className="subTitle">{this.props.atividade.local}</p>
                    {this.props.atividade.percurso != null ? <div>
                    <p className="title uppercase">percurso</p>
                    <p className="infoText">{'- '+this.props.atividade.percurso.nome}</p>
                    {/* <span onClick={this.goToPercurso(this.props.atividade.percurso.id)} className="seePresenca">ver percurso</span> */}
                    </div> : null }
                    {this.props.atividade.tipo == 'avaliacao' ? <div><p className="title uppercase">nota</p>
                    <p className="infoDescription">{'Peso da avaliação' + this.props.atividade.notaMaxima}</p>
                    <p className="infoDescription">{'Sua nota' + this.props.presenca.nota}</p></div> : null}
                    <p className="title uppercase">observações</p>
                    <p className="infoText">{this.props.presenca.observacao}</p>
                    <Button dark={true} label={this.props.buttonLabel} active={false}/>
                </section>
                <section className="secondSection">
                    <p className="title uppercase">descrição</p>
                    <p className="infoDescription">{this.props.atividade.descricao}</p>
                </section>
            </div> 
        );
    }
}

export default index;