import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import Button from '../button'
class index extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            show: false,
            observacao: "",
            nota: "",
         };
    }
    updatePresenca = () => {

    }
    modal = () => {
        const user = this.props.usuario
        return (
            <div className="modal">
                <Button dark={false} label="salvar" action="salvar" handleClick={this.updatePresenca} active={false}/>
                <p className="infoText">{user.nomeCompleto + ' - ' + user.categoria.categoria}</p>
                <Link to={{pathname: "/viewProfile",state: {usuario: user}}} >ver mais</Link>

            </div>
        )
    }
    render() {
        return (
            <div>{
                this.state.show ? this.modal() : <button className="underlineButton" onClick={() => {this.setState((prevState,props) => {return {show: !prevState.show}})}}>ver mais</button>
            }</div>
        );
    }
}

export default index;