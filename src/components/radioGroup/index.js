import React from 'react';

const index = (props) => {
    const radioButtons = () => {
        return props.options.map(radio => {
            return (
            <div className="radioButton" key={radio.value}>
                <input checked={radio.checked} onChange={props.handleSelect} className="radioCheck" type="radio" name={radio.name} value={radio.value} id={radio.value}/>
                <label className="capitalize" htmlFor={radio.value}>{radio.label}</label>
            </div>
            )
        })
    }
    return (
        <div className="radioGroup">
            {radioButtons()}
        </div>
    );
}

export default index;