import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import Login from '../login';
import Navigation from '../navbar'
import Cadastro from '../cadastrarUsuario'
import CadastroDeAtividade from '../cadastrarAtividade'
import CadastrarPercurso from '../cadastrarPercurso' 
import Agenda from '../agenda'
import CadastroCategoria from '../cadastrarCategoria'
import CadastrarUsuario from  '../cadastrarUsuario'
import Dia from '../day'
import Perfil from '../perfil'
import Diario from '../diario'
import Percursos from '../percursos'
import GerenciarAtividades from '../gerenciarAtividades'
import UpdateAtividade from '../updateAtividade'
class index extends Component {
    constructor(props) {
        super(props)
        this.state ={
            logged: props.auth.userLogado()
        }
    }
    doLogin = (user) =>{
        this.props.auth.login(user)
        this.setState({
            logged: this.props.auth.userLogado()
        })
    }
    logout = () => {
        this.props.auth.logout()
        this.setState({
            logged: this.props.auth.userLogado()
        })
    }
    render(){return (
        <div>
            {this.state.logged ? <BrowserRouter>
          <Navigation logout={this.logout}/>
          <Route exact path='/' component={Agenda} />
          <Route path='/cadastroCategoria' component={CadastroCategoria} />
          <Route path='/cadastroAtividade' component={CadastroDeAtividade} />
          <Route path='/cadastroPercurso' component={CadastrarPercurso} />
          <Route path='/cadastroUsuario' component={CadastrarUsuario} />
          <Route path='/cadastro' component={Cadastro} />
          <Route path='/dia' component={Dia} />
          <Route path='/perfil' component={Perfil} />
          <Route path='/diario' component={Diario} />
          <Route path='/percursos' component={Percursos} />
          <Route path='/gerenciarAtividades' component={GerenciarAtividades} />
          <Route path='/editarAtividade' component={UpdateAtividade}/>
        </BrowserRouter> : <Login doLogin={this.doLogin}/>}
        </div>
    );}
}

export default index;