import React from 'react';

const index = (props) => {
    const options = () => {
        return props.options.map(o => {
            return <option key={o.id} value={o.id}>{o.categoria}</option>
        })
    }
    return (
        <select name={props.name} onChange={props.handleChange} className="select">
            <option>categoria de usuario</option>
            {options()}
        </select>
    );
}

export default index;