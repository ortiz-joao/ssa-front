import React, { Component } from 'react';
import Button from '../button'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showDetails: false
        }
    }
    pastActivity() {
        const d = new Date(this.props.atividade.horarioDeTermino)
        const today = new Date()
        return today.getTime() > d.getTime()
     }
    addZero = (i) => {
        if (i < 10) {
             i = "0" + i;
         }
         return i;
     }
    cardHorario = (inicio, fim) => {
         const i = new Date(inicio)
         const f = new Date(fim)
         return this.addZero(i.getHours()) + ':' + this.addZero(i.getMinutes()) + ' - ' + this.addZero(f.getHours()) + ':' + this.addZero(f.getMinutes())
     }
    handleSubscribe = () => {
 
     }
     seeActivity = () => {
 
     }
    render() {
        return (
            <div className="cardContainer">
                {this.pastActivity() ? <Button dark={false} label="indisponivel" action="indisponivel" handleClick={this.handleSubscribe} active={false}/> : <Button dark={false} label="participar" action="participar" handleClick={this.handleSubscribe} active={false}/>}
                <div className="cardInfo">
                    <div className="horario"> {this.cardHorario(this.props.atividade.horarioDeInicio, this.props.atividade.horarioDeTermino)}</div>
                    <div className="nomeDaAtividade">{this.props.atividade.nome}</div>
                    <div className="tipo-materia">{this.props.atividade.tipo + ' - ' + this.props.atividade.materia.disciplina}</div>
                </div>
                <span className="seeActivityDetails" onClick={this.seeActivity}>ver mais</span>
                {/* {showDetails ? <ActivityDetails /> : null} */}
            </div>
        );
    }
}

export default index;