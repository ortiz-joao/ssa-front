import React from 'react';

const index = (props) => {
    const options = () => {
        return props.options.map(o => {
            return <button   onClick={() => {
                props.handleChange(o.value)
            }} className={props.initial == o.value ? "option uppercase active": "option uppercase"}>{o.label}</button>
        })
    }

    return (
        <div className="tabs">
            {options()}
        </div>
    );
}

export default index;