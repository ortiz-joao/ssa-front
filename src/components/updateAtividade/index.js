import React, { Component } from 'react';
import Input from '../inputField'
import Button from '../button'
import RadioGroup from '../radioGroup'
import AddOptions from '../addOptions'
import Checkbox from '../checkbox'
import Horarios from '../horarios'
import _ from 'lodash'
import { graphql } from 'react-apollo';
import {atividade,updateAtividade,addPresencaToUsuario, addAtividadeToPercurso ,percursosDoProfessor , addAtividade , categoriasDeUsuarios} from '../../queries/queries'
import MaskedInput from 'react-text-mask'
import moment from 'moment'
const user = JSON.parse(localStorage.getItem('user'))
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {tipo: props.history.location.state.atividade.tipo,
            nome: props.history.location.state.atividade.nome, 
            local: props.history.location.state.atividade.local,
            notaMaxima: props.history.location.state.atividade.notaMaxima,
            limiteDeParticipantes: props.history.location.state.atividade.limiteDeParticipantes,
            categoriasDeUsuariosId: [],
            disciplina: null,
            descricao: props.history.location.state.atividade.descricao,
            duracao: null,
            horarioInicio: props.history.location.state.atividade.horarioInicio,
            horarioTermino: props.history.location.state.atividade.horarioTermino,
            horarios: [{horario:moment(props.history.location.state.atividade.horarioInicio, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm'), dia:moment(props.history.location.state.atividade.horarioInicio, 'YYYY-MM-DDTHH:mm:ss').format('DD/MM')}],
            percurso: [props.history.location.state.atividade.percursosDaAtividade],
            user: JSON.parse(localStorage.getItem('user'))
        }
        this.props = props
    }
     handleChange = (e) => {
         this.setState({
             [e.target.id]: e.target.value
         })
     }
     handleSubmit = (e) => {
         e.preventDefault()
        this.state.horarios.map(data => {
            const dia = moment(data.dia+'/2019',"DD-MM-YYYY").format("YYYY-MM-DD")
            const duracao = this.state.duracao.split(':')
            const horarioInicio = moment(dia+' '+ data.horario, "YYYY-MM-DD HH:mm").format('YYYY-MM-DD hh:mm')
            const horarioTermino = moment(horarioInicio, 'YYYY-MM-DD hh:mm').add(duracao[0], 'h').add(duracao[1],'m').format('YYYY-MM-DD hh:mm')
            this.props.addAtividade({
                variables: {
                    data:dia,
                    nome:this.state.nome,
                    descricao:this.state.descricao,
                    horarioInicio: horarioInicio,
                    horarioTermino: horarioTermino,
                    tipo: this.state.tipo,
                    local: this.state.local,
                    limiteDeParticipantes: parseInt(this.state.limiteDeParticipantes),
                    notaMaxima: this.state.notaMaxima
                }
            }).then(a => {
                const atividade = a.data.addAtividade
                this.props.addPresencaToUsuario({
                    variables: {
                        usuarioId: parseInt(this.state.user.id),
                        atividadeId: atividade.id,
                        papel: 'responsavel',
                        estado: 'presente'
                    }
                })
                this.state.percurso.map(p => {
                    this.props.addAtividadeToPercurso({
                        variables:{
                            atividadeId: atividade.id,
                            percursoId: parseInt(p.id)
                        }
                    })
                })
            })
        })
     }
     handleRadio= (e) => {
         console.log(e)
        this.setState({
            tipo: e.target.value
        })
     }
     handleCategorias = (e) => {

     }
     handlePercurso = (e) => {
        this.setState({
            percurso: e
        })
     }
     handleMateria = (e) =>{
        this.setState({disciplina: e} 
        )
    }
        tiposDisponiveis = () => {
            switch (user.tipo) {
                case "aluno":
                    return [{name:"tipo", value: "atividadePessoal", label: "atividade Pessoal"}]
                case "professor":
                     return   [{name:"tipo", value: "aula", label: "aula"},{name:"tipo", value: "avaliacao", label: "avaliação"},{name:"tipo", value: "atividadePessoal", label: "atividade Pessoal"}]
                case "gestor":
                     return   [{name:"tipo", value: "aula", label: "aula"},{name:"tipo", value: "avaliacao", label: "avaliação"},{name:"tipo", value: "atividadePessoal", label: "atividade Pessoal"}]
                default:
                    break;
            }
        }
     tipoOptions = () => {
         switch (this.state.tipo){
            case "avaliacao":
                 return <Input type="text" name="notaMaxima" label="peso da avaliação" handleChange={this.handleChange}/>
            // case "professor":
            //     return (
            //         <AddMateria />
            //         // <AddGrupo />
            //     )
            // case "responsavel":
            //     return <AddAluno />
         }
     }
     handleCheck = (e) => {
        let c = this.state.categoriasDeUsuariosId
        if (c.filter(id => {return id == e.target.value}).length > 0) {
            let categoriasDeUsuariosId = c.filter(id => {
                return id != e.target.value
            })
            this.setState({
                categoriasDeUsuariosId
            })
        } else {
            let categoriasDeUsuariosId = [...c,e.target.value]
            this.setState({
                categoriasDeUsuariosId
            })
        }
     }
     handleHorarios = (e) => {
        let h = this.state.horarios
        if (h.filter(hr => {return hr.dia == e.dia && hr.horario == e.horario}).length == 0) {
            let horarios = [...h,e]
            this.setState({
                horarios
            })
        }
     }
     editarHorario = (e,n) => {
         console.log(e);
         console.log(n);
        let h = this.state.horarios.filter(hr => {return hr.dia != e.dia && hr.horario != e.horario})
            console.log(h);
            let horarios = [...h,n]
            this.setState({
                horarios
            })
     }
     removerHorario = (e) => {
        let horarios = this.state.horarios.filter(hr => {return hr.dia != e.dia && hr.horario != e.horario})
            this.setState({
                horarios
            })
        }
     categorias = () => {
         let categoriasF = [{categoria: 'meda',id: 1}]
         return categoriasF.map(c => {
            return (<Checkbox key={c.id} onChange={(event) => this.handleCheck(event)} name={c.categoria} label={c.categoria} value={c.id}/>)
         })
     }
     form = () => {
        return <div>
        {/* <section>
            <p className="title uppercase">escolha o tipo de atividade
            </p>
            <RadioGroup options={this.tiposDisponiveis()} handleSelect={this.handleRadio} />
            <p className="title uppercase">quando acontecerá</p>
            <Horarios removerHorario={this.removerHorario} editarHorario={this.editarHorario} horarios={this.state.horarios} addHorario={this.handleHorarios}/>
            {this.state.user.tipo == "professor" && (this.state.tipo == "aula" || this.state.tipo == "avaliacao") ? <AddOptions handleSelect={this.handlePercurso} selectedOptions={this.state.percurso} options={this.props.percursosDoProfessor.loading ? [] : this.props.percursosDoProfessor.professor.percursos}  title="percurso da atividade" optionId="id" optionLabel="nomeCompleto" /> : null}
            <p className="title uppercase">quem podera participar</p>
        </section>
        <section>
            <form className="fomrContainer" onSubmit={this.handleSubmit}>
                <Input type="text" name="nome" label="nome da atividade" handleChange={this.handleChange}/>
                <Input type="text" name="local" label="local" handleChange={this.handleChange}/>
                {this.tipoOptions()}
                <MaskedInput mask={[/\d/,/\d/,':',/\d/,/\d/]} ref="duracao" className="horarioText" id="duracao" name="duracao" onChange={this.handleChange}/>
                <label htmlFor="duracao" className="inputLabel">duracao</label>
                <Input type="text" name="limiteDeParticipantes" label="capacidade de participantes" handleChange={this.handleChange}/>
                <Input type="text" name="descricao" label="descrição" handleChange={this.handleChange}/>
                <Button dark={false} label="agendar" action="agendar" handleClick={this.handleSubmit} active={false}/>
            </form>
        </section> */}
    </div>
     }
    render() {
        return (
            !this.props.data.loading ? this.form() : <p>carregando</p>
        );
    }
}

export default _.flowRight(
    graphql(atividade,{options: (props) => { return {name:"atividade", variables: {id:parseInt(props.history.location.state.atividadeId)}}}}),
    graphql(updateAtividade, {name: "updateAtividade"}),
    graphql(addPresencaToUsuario, {name: "addPresencaToUsuario"}),graphql(addAtividadeToPercurso, {name: "addAtividadeToPercurso"}),
    graphql(addAtividade, {name: "addAtividade"}),graphql(categoriasDeUsuarios, {name: "categorias"}),
    graphql(percursosDoProfessor,{name: "percursosDoProfessor",variables: {id: user.professor != null ? user.professor.id : null }})
    )(index);