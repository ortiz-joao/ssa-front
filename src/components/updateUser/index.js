import React, { Component } from 'react';
import Input from '../inputField'
import Button from '../button'

class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tipo: null,
            nomeDeUsuario: null,
            nomeCompleto: null,
            email: null,
            turma: null,
            categoriaDeUsuarioId: null,
            senha: null,
            responsaveis: null,
            materias: null,
            responsavelPor: null,
            orientandos: null
        }
        this.props = props
    }
     handleChange = (e) => {
         this.setState({
             [e.target.id]: e.target.value
         })
     }
     handleSubmit = (e) => {
         e.preventDefault()
         const user = this.state
         this.props.doLogin(user)
     }
    render() {
        return (
            <div>
                <h2 className="pageTitle bitText">atualizar dados</h2>
                <section>
                    
                </section>
                <section>
                    <form className="loginContaienr" onSubmit={this.handleSubmit}>
                        <Input type="text" name="nomeDeUsuario" handleChange={this.handleChange}/>
                        <Input type="text" name="nomeCompleto" handleChange={this.handleChange}/>
                        <Input type="text" name="email" handleChange={this.handleChange}/>
                        {this.state.tipo == "aluno" ? <Input type="text" name="username" handleChange={this.handleChange}/> : null}
                        <Input type="password" name="senha" handleChange={this.handleChange}/>
                        <Button dark={false} label="atualizar" action="atualizar" handleClick={this.handleSubmit} active={false}/>
                    </form>
                </section>
            </div>
            
        );
    }
}

export default index;