import React, { Component } from 'react';
import PresenceActivityCard from '../PresenceActivityCard'
class index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showPresencas = false,
            showBolteim = false
        }
    }
    render() {
        return (
            <div className="profileContainer">
                <section className="userInfo">
                    <span className="undelineButton editUser">EDITAR</span>
                    <h2 className="nome"></h2>
                    <p className="title"></p>
                    <p className="title uppercase"></p>
                    <div className="responsaveis">
                        
                    </div>
                </section>
                <section class="userActivities">
                    <div className="atividadesMarcadas">
                        <div className="header">
                            <span className="subTitle uppercase">atividades marcadas</span>
                            {/* changeday */}
                            {/* <PresenceActivityCard /> */}
                        </div>
                        <div className="atividadesRecentes">
                            <span className="subTitle uppercase">atividades recentes</span>
                            {/* <PresenceActivityCard /> */}
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default index;