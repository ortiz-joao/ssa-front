import { gql } from 'apollo-boost'

const addCategoriaDeUsuario = gql`
    mutation addCategoriaDeUsuario($categoria: String!){
        addCategoriaDeUsuario(categoria: $categoria){
            id
            categoria
        }
    }
`
const alunos =gql`
    {
        alunos{
            id
            turma
            usuario{
                nomeCompleto
                id
                nomeCompleto
            }
        }
    }
`
const responsaveis =gql`
        {responsaveis{
            id
            usuario{
                nomeCompleto
                nomeDeUsuario
                id
            }
        }}
`


const addDisciplina = gql`
    mutation addDisciplina($disciplina: String!){
        addDisciplina(disciplina: $disciplina){
            id
            disciplina
        }
    }
`
const addPercurso =gql`
    mutation addPercurso($nome: String!, $descricao: String!,$limiteDeParticipantes: Int!) {
        addPercurso(nome: $nome,descricao: $descricao, limiteDeParticipantes:$limiteDeParticipantes){
            id
            nome
        }
    }
`

const percurso = gql`
query percurso($id: Int){
    percurso(id: $id){
                id
                nome
                descricao
                limiteDeParticipantes
                atividadesDoPercurso{
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
            }
                professores{
                    usuario{
                        nomeCompleto
                    }
                }
                alunos{
                    id
                }
            }
}
`

const updatePresenca = gql`
mutation updatePresenca($atividadeId: Int,$usuarioId: Int, $estado: PresencaEstadoEnum, $papel: PresencaPapelEnum, $observacao: String, $nota: Float){
    updatePresenca(atividadeId: $atividadeId,usuarioId: $usuarioId,estado: $estado, papel: $papel, observacao: $observacao,nota: $nota){
        estado
    }
}
`

const percursos = gql`
{
    percursos{
                id
                nome
                descricao
                limiteDeParticipantes
                atividadesDoPercurso{
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
            }
                professores{
                    usuario{
                        nomeCompleto
                    }
                }
                alunos{
                    id
                }
            }
}
`

const percursosDoProfessor =gql`
    query professor($id: Int){
        professor(id: $id){
            id
            percursos{
                id
                nome
                descricao
                limiteDeParticipantes
                atividadesDoPercurso{
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
            }
            }
        }
    }
`

const percursosDoAluno =gql`
    query aluno($id: Int){
        aluno(id: $id){
            id
            percursosDoAluno{
                id
                nome
                descricao
                limiteDeParticipantes
                atividadesDoPercurso{
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
            }
                professores{
                    usuario{
                        nomeCompleto
                    }
                }
                alunos{
                    id
                }
            }
        }
    }
`



const atividadesMarcadas =gql`
query presencas($data: String, $usuarioId: Int){
    presencas(data: $data, usuarioId: $usuarioId){
        data
        papel
        estado
        observacao
        nota
        atividade{
            id
            nome
            horarioInicio
            horarioTermino
            tipo
            data
            percursoDaAtividade{
                nome
            }
        }
    }
}
`

const atividade = gql`
    query atividade($id: Int){
    atividade(id: $id){
        id
        nome
        descricao
        notaMaxima
        horarioInicio
        horarioTermino
        tipo
        local
        limiteDeParticipantes
        data
        presencas{
            estado
            papel
            observacao
            usuario{
                id
                nomeCompleto
            }
        }
        percursoDaAtividade{
            id
            nome
        }
    }
}
`

const updateAtividade = gql`
mutation updateAtividade($data: String!,$nome: String!, $descricao: String!, $horarioInicio: String!, $horarioTermino: String!, $tipo: TipoAtividadeEnum,$local: String!,$limiteDeParticipantes: Int!){
    updatePresenca(data: $data,nome: $nome,descricao: $descricao, horarioInicio: $horarioInicio, horarioTermino: $horarioTermino, tipo: $tipo, local: $local, limiteDeParticipantes: $limiteDeParticipantes){
        data
        nome
        descricao
        tipo
    }
}
`

const atividadesDisponiveisDoAluno = gql`
query atividadesDisponiveisDoPercurso($usuarioId: Int, $data: String){
    usuario(id: $usuarioId){
        categoria{
            atividadesDisponiveis(data: $data){
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
                participantes{
                    id
                }
                percursoDaAtividade{
                    nome
            }
        }
        }
        aluno{
            percursosDoAluno{
                atividadesDoPercurso(data: $data){
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
                participantes{
                    id
                }
                percursoDaAtividade{
                    nome
            }
        }
            }
        }
    }
}
`


const atividadesDoUsuario =gql`
    query atividadesDoUsuario($usuarioId: Int,$mes: Int,$papel: PresencaPapelEnum){
        dias(mes: $mes){
            data
            dataString
            dia
            mes
            ano
            diaDaSemana
            presencas(usuarioId: $usuarioId, papel: $papel){
                papel
                estado
                atividadeId
                atividade{
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
                notaMaxima
                limiteDeParticipantes
                percursoDaAtividade{
                    nome
                }
                presencas{
                    estado
                    papel
                    usuario{
                        id
                        nomeCompleto
                        categoria {
                            categoria
                        }
                    }
                }
            }
            }
        }
    }
`

const atividadesRecentes = gql`
    query atividadesRecentes($usuarioId: Int!){
        atividadesRecentes(usuarioId: $usuarioId){
            data
            papel
            estado
            observacao
            nota
            atividade{
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
                notaMaxima
                limiteDeParticipantes
                percursoDaAtividade{
                    nome
                }
            }
        }
    }
`

const currentUser = gql`
    query MyCurrentUser{
        currentUser{
            id
            nomeDeUsuario
            nomeCompleto
            tipo
            email
            categoria{
                id
                categoria
            }
            responsavel{
                id
            }
            aluno{
                id
                turma
            }
            professor{
                id
            }
            gestor{
                id
            }
        }
    }
`
const userLogin =gql`
    mutation login($username: String!,$password: String!){
        login(username: $username,password: $password){
            id
            nomeDeUsuario
            nomeCompleto
            tipo
            email
            categoria{
                id
                categoria
            }
            responsavel{
                id
            }
            aluno{
                id
                turma
            }
            professor{
                id
            }
            gestor{
                id
            }
        }
    }
`

const addProfessorToGrupo = gql`
    mutation addProfessorToGrupo($professorId: Int!, $grupoId: Int!){
        addProfessorToGrupo(professorId: $professorId, grupoId: $grupoId){
            id
        }
    }
`

const removePercursoFromAluno= gql`
mutation removePercursoFromAluno($alunoId: Int, $percursoId: Int){
    removePercursoFromAluno(alunoId: $alunoId,percursoId: $percursoId){
        id
    }
}`

const addPercursoToAluno = gql`

    mutation addPercursoToAluno($alunoId: Int!,$percursoId: Int!){
        addPercursoToAluno(alunoId: $alunoId, percursoId: $percursoId){
            id
        }
    }
`

const addPercursoToProfessor =gql`
mutation addPercursoToProfessor($professorId: Int,$percursoId: Int){
    addPercursoToProfessor(professorId:$professorId,percursoId: $percursoId){
        id
    }
}
`

const addAlunoToGrupo = gql`
    mutation addAlunoToGrupo($alunoId: Int!, $grupoId: Int!){
        addAlunoToGrupo(alunoId: $alunoId, grupoId: $grupoId){
            id
        }
    }
`

const removePresencaFromUser =gql`
mutation removePresencaFromUser($usuarioId: Int, $atividadeId: Int){
    removePresencaFromUser(usuarioId:$usuarioId,atividadeId:$atividadeId){
        estado
    }
}
`

const addPresencaToUsuario =gql`

    mutation addPresencaToUsuario($atividadeId: Int!,$usuarioId: Int!,$nota: Float,$observacao: String,$estado: PresencaEstadoEnum!,$papel: PresencaPapelEnum!){
        addPresencaToUsuario(atividadeId: $atividadeId, usuarioId: $usuarioId, nota: $nota, observacao: $observacao, estado: $estado, papel: $papel){
            data
        }
    }
`
const addAtividade = gql`

    mutation addAtividade($data: String!,$nome: String!, $descricao: String!, $horarioInicio: String!, $horarioTermino: String!, $tipo: TipoAtividadeEnum,$local: String!,$limiteDeParticipantes: Int!){
        addAtividade(data: $data,nome: $nome,descricao: $descricao, horarioInicio: $horarioInicio, horarioTermino: $horarioTermino, tipo: $tipo, local: $local, limiteDeParticipantes: $limiteDeParticipantes){
            id
            nome
            data
            descricao
            local
            tipo
        }
    }
`

const addAtividadeToPercurso = gql`
mutation addAtividadeToPercurso($atividadeId: Int, $percursoId: Int){
    addAtividadeToPercurso(atividadeId: $atividadeId, percursoId: $percursoId){
        nome
    }
}
`

const addResponsavelToAluno = gql`
    mutation addResponsavelToAluno($alunoId: Int!,$responsavelId: Int!){
        addResponsavelToAluno(alunoId: $alunoId, responsavelId: $responsavelId){
            id
            turma
        }
    }
`

const categoriasDeUsuarios = gql`
    {
        categoriasDeUsuarios{
            id
            categoria
        }
    }
`

const alunosAndName = gql`
    {
        alunos{
            id
            turma
            usuario{
                nomeCompleto
            }
        }
    }
`
const disciplinas =gql`
 {
     disciplinas{
         disciplina
         id
     }
 }
`
const responsaveisAndName =gql`
{
    responsaveis{
        id
        usuario{
            nomeCompleto
        }
    }
}
`
const grupos =gql`
{
    grupos{
        id
    }
}`

const addUsuario =gql`
mutation addUsuario($tipo: String!, $nomeDeUsuario:String!, $nomeCompleto: String!, $email: String!, $senha:String!, $turma: String, $categoriaDeUsuarioId: Int!){
    addUsuario(tipo: $tipo, nomeDeUsuario: $nomeDeUsuario, nomeCompleto: $nomeCompleto, email : $email, senha:$senha, turma: $turma, categoriaDeUsuarioId: $categoriaDeUsuarioId){
        id
        nomeCompleto
        nomeDeUsuario
        tipo
        email
        aluno{
            id
            turma
        }
        responsavel{
            id
        }
        professor{
            id
        }
    }
}
`

const dias = gql`
    {
        dias{
            data
            dataString
            dia
            mes
            ano
            diaDaSemana

        }
    }
`

const diario = gql`
query diario($id: Int,$mes: Int){
    dias(mes: $mes){
        data
        dataString
            dia
            mes
            ano
            diaDaSemana
        diario(usuarioId: $id){
           papel
           estado
           observacao
           nota
           data
           atividade{
                id
                nome
                horarioInicio
                horarioTermino
                tipo
                data
                notaMaxima
                limiteDeParticipantes
                percursoDaAtividade{
                    nome
                }
            }
        }
    }
}
`

export {updateAtividade,updatePresenca,percurso,removePresencaFromUser,addPercursoToProfessor,addPercursoToAluno,percursos,percursosDoAluno,diario, atividadesDisponiveisDoAluno, addPresencaToUsuario,addAtividadeToPercurso ,atividadesDoUsuario, addAtividade ,percursosDoProfessor,atividade,atividadesMarcadas, atividadesRecentes,currentUser,userLogin ,addCategoriaDeUsuario,removePercursoFromAluno, dias, categoriasDeUsuarios, addPercurso, addUsuario, addResponsavelToAluno, grupos, alunos, responsaveis, addAlunoToGrupo, addProfessorToGrupo};